/**
 * OpenKM, Open Document Management System (http://www.openkm.com)
 * Copyright (c) 2006-2013 Paco Avila & Josep Llort
 * 
 * No bytes were intentionally harmed during the development of this application.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package pe.blobfish.signature;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.asn1.x509.CRLReason;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoGeneratorBuilder;
import org.bouncycastle.tsp.*;
import org.bouncycastle.x509.X509V1CertificateGenerator;
import org.bouncycastle.x509.X509V2CRLGenerator;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.hamcrest.Matcher;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import pe.blobfish.certutils.CertificateUtil;

import javax.security.auth.x500.X500Principal;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

// import org.bouncycastle.cert.X509v3CertificateBuilder;

// TODO create a test case for each widely used signature format/subfilter on PDFs
// TODO all PDFs (signed, timestamped, etc) should be created at runtime for testing, create a utility to do so.
public class OKMPDFSignatureTest extends BaseTests {
	
	private X509Certificate untrustedCACertificate;
	private PrivateKey untrustedCAPrivateKey;
	// private X500Principal rootCASubject;
	private PublicKey endEntityPublicKey;
	private PrivateKey endEntityPrivateKey;
	private X509Certificate trustedIntermediateCACert;
	private PrivateKey trustedIntermediateCAKey;
	
	// static to be able to shut it down in the setup method of the next test case, it seems that the test class is
	// being instantiated for each test case being executed... is this the normal junit behaviour??
	private static HttpServer intermediateCRLHttpServer;
	private static HttpServer rootCRLHttpServer;
	
	private PrivateKey trustedRootCAKey;
	private X509Certificate trustedRootCACert;
	
	/**
	 * Receives a filename (located in classpath) and performs signature validation using Blobfish Signer API.
	 * 
	 * This is documented displaying the principal steps in signature validation and error management.
	 * 
	 * @param file
	 */
	private void verifyFile(String file) {
		System.out.println("-------- Verifying file " + file + "--------");
		// call API
		SignatureResult result = pdfSignatureValidator.verify(OKMPDFSignatureTest.class.getResourceAsStream(file));
		
		// get validation results
		SignatureResult.CryptographicSignatureStatus.Status validSignature = result.getCryptographicSignatureStatus().getStatus();
		X509Certificate signerCertificate = result.getSignerCertificate();
		
		// process validation results
		if (validSignature == SignatureResult.CryptographicSignatureStatus.Status.VALID) {
			
			// a. if signature is valid display signature details
			System.out.println("File: " + file + ", " + result);
		} else {
			// b. if signature is not valid display the error reason
			System.out.println("File: " + file + ", " + result + ", reason: " + result.getCryptographicSignatureStatus().getMessage());
		}
		System.out.println("  --- Signature properties ---");
		if (result.getSignerCertificate() != null) {
			System.out.println("    -- Signer ("
					+ ((result.getSignerVerificationStatus().getTrustStatus() != null && result.getSignerVerificationStatus()
							.getTrustStatus().getStatus() == SignatureResult.TrustStatus.Status.TRUSTED) ? "Trusted" : "Untrusted")
					+ ") --");
			
			System.out.println("      Name: " + CertificateUtil.getSubjectName(result.getSignerCertificate()));
			System.out.println("      Organization: " + CertificateUtil.getSubjectOrganization(result.getSignerCertificate()));
			System.out.println("      - Issuer -");
			System.out.println("        Name: " + CertificateUtil.getIssuerName(result.getSignerCertificate()));
			System.out.println("      - End of Issuer -");
			System.out.println("    -- End of Signer --");
		}
		System.out.println("    Signing time: " + result.getSigningTime());
		System.out.println("    Has timeStampToken?: " + result.isSigningTimeFromTimeStampToken());
		System.out.println("    Signing reason: " + result.getSigningReason());
		System.out.println("    Signing location: " + result.getSigningLocation());
		System.out.println("  --- End of Signature properties ---");
		System.out.println("-------- Finished veryfing file " + file + "--------");
	}
	
	private OKMPDFSignature pdfSignatureValidator;
	
	@Before
	public void setUp() throws Exception {
		
		pdfSignatureValidator = instantiateOKMITextPDFSignature();
		
		// TODO evaluate encapsulate this code for reuse in BaseTests
		
		// TODO some code here should use a test class initializer as its inmutable
		// create root key pair and self signed certificate
		KeyPair caKeyPair = generateRSAKeyPair();
		X500Principal rootCASubject = new X500Principal("CN=issuer");
		X509V1CertificateGenerator x509V1CertificateGenerator = new X509V1CertificateGenerator();
		x509V1CertificateGenerator.setSerialNumber(BigInteger.TEN);
		x509V1CertificateGenerator.setIssuerDN(rootCASubject);
		x509V1CertificateGenerator.setSubjectDN(rootCASubject);
		x509V1CertificateGenerator.setNotBefore(new DateTime().minusHours(2).toDate());
		x509V1CertificateGenerator.setNotAfter(new DateTime().plusHours(2).toDate());
		x509V1CertificateGenerator.setPublicKey(caKeyPair.getPublic());
		x509V1CertificateGenerator.setSignatureAlgorithm("sha1withrsa");
		untrustedCACertificate = x509V1CertificateGenerator.generate(caKeyPair.getPrivate());
		untrustedCAPrivateKey = caKeyPair.getPrivate();
		
		KeyPair endEntityKeyPair = generateRSAKeyPair();
		endEntityPublicKey = endEntityKeyPair.getPublic();
		endEntityPrivateKey = endEntityKeyPair.getPrivate();
		
		// TODO create and sign trusted intermediate
		KeyStore instance = KeyStore.getInstance("JKS");
		instance.load(OKMPDFSignatureTest.class.getResourceAsStream("/truststore_with_root_key.jks"), "changeit".toCharArray());
		String alias = "test_trusted_root_ca";
		trustedRootCAKey = (PrivateKey) instance.getKey(alias, "".toCharArray());
		trustedRootCACert = (X509Certificate) instance.getCertificate(alias);
		
		KeyPair trustedIntermediateKeyPair = generateRSAKeyPair();
		trustedIntermediateCAKey = trustedIntermediateKeyPair.getPrivate();
		trustedIntermediateCACert = generateIntermediateCertificate(trustedRootCAKey, trustedIntermediateKeyPair.getPublic(),
				trustedRootCACert.getSubjectX500Principal(), new X500Principal("CN=test trusted Intermediate CA"),
				"http://localhost:10002/rootcrl");
		
		if (intermediateCRLHttpServer != null) { // if it is created ensure it is off
			intermediateCRLHttpServer.stop(1);
		}
		
		if (rootCRLHttpServer != null) { // if it is created ensure it is off
			rootCRLHttpServer.stop(1);
		}
	}
	
	private X509Certificate generateIntermediateCertificate(PrivateKey rootKey, PublicKey intermediatePublicKey, X500Principal issuerDN,
			X500Principal subjectDN, String crlDistPoint) throws CertificateEncodingException, NoSuchAlgorithmException,
			SignatureException, InvalidKeyException {
		X509V3CertificateGenerator x509V3CertificateGenerator = new X509V3CertificateGenerator();
		x509V3CertificateGenerator.setIssuerDN(issuerDN);
		x509V3CertificateGenerator.setSerialNumber(BigInteger.ONE);
		x509V3CertificateGenerator.setNotBefore(DateTime.now().minusDays(1).toDate());
		x509V3CertificateGenerator.setNotAfter(DateTime.now().plusDays(1).toDate());
		x509V3CertificateGenerator.setSubjectDN(subjectDN);
		x509V3CertificateGenerator.setPublicKey(intermediatePublicKey);
		x509V3CertificateGenerator.setSignatureAlgorithm("SHA1WITHRSA");
		if (crlDistPoint != null) {
			x509V3CertificateGenerator.addExtension(
					X509Extension.cRLDistributionPoints,
					false,
					new CRLDistPoint(new DistributionPoint[] { new DistributionPoint(new DistributionPointName(new GeneralNames(GeneralName
							.getInstance(new DERTaggedObject(6, new DERIA5String(crlDistPoint))))), null, null) }));
		}
		
		x509V3CertificateGenerator.addExtension(X509Extension.keyUsage, true, new KeyUsage(KeyUsage.keyCertSign | KeyUsage.cRLSign));
		
		x509V3CertificateGenerator.addExtension(X509Extension.basicConstraints, true, new BasicConstraints(0));
		
		return x509V3CertificateGenerator.generate(rootKey);
	}
	
	/**
	 * @verifies verify revocation status at the time of signing for all certs in certification path
	 * @see OKMPDFSignature#verify(java.io.InputStream)
	 */
	@Test
	public void verify_shouldVerifyRevocationStatusAtTheTimeOfSigningForAllCertsInCertificationPath() throws Exception {
		X509CRL rootCRL = generateCRL(trustedRootCAKey, trustedRootCACert);
		
		rootCRLHttpServer = publishCRLWithHttpServer(rootCRL, "/rootcrl", 10002);
		
		/////////
		// CRL revoking trusted certificate
		////////////////////
		X509Certificate trustedEndEntityCertificate = generateEndEntityX509Certificate(BigInteger.ONE, "CN=subject", endEntityPublicKey,
				trustedIntermediateCACert, trustedIntermediateCAKey, true, false);
		
		// create crl revoking that certificate, sign it with root cert
		BigInteger revokedSerialNumber = BigInteger.ONE;
		X509CRL generatedCRL = generateCRL(trustedIntermediateCAKey, trustedIntermediateCACert, revokedSerialNumber);
		intermediateCRLHttpServer = publishCRLWithHttpServer(generatedCRL, "/intermediatecrl", 10001);
		byte[] signedPDFByteArray = generateSignedPDF(endEntityPrivateKey, new Certificate[] { trustedEndEntityCertificate,
				trustedIntermediateCACert });
		
		// assert invalid signature and error reason
		SignatureResult signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage(),
				startsWith("Signer certificate revoked at"));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.REVOKED));
		
		////////////////////////////////////////////////////////////////////////////////
		// if crl dist point exist and doesn't revoke certificate return OK status
		////////////////////////////////////////////////////////////////////////////////
		intermediateCRLHttpServer.stop(1);
		generatedCRL = generateCRL(trustedIntermediateCAKey, trustedIntermediateCACert);
		intermediateCRLHttpServer = publishCRLWithHttpServer(generatedCRL, "/intermediatecrl", 10001);
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.OK));
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		
		///////////////////////////////////////////////////////
		// if crl is unavailable from first dist. point return valid signature but unknown status, TODO it should be
		// able to query more than just one CDP URI from a certificate, test it!
		//////////////////////////////////////////////////////
		intermediateCRLHttpServer.stop(1);
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.UNKNOWN));
		
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		
		/////////////////////////////////////
		// if signature was made before certificate get revoked by a 'malicious' reason code REVOKED status, see:
		// http://csrc.nist.gov/nissc/1998/proceedings/paperG2.pdf. In this situation a timestamp over the signature
		// could help the subscriber to prove he actually made the signature some time ago (how much would depend on the
		// policy) the key got revoked by the 'malicious' reason (under the supervision of a judge)
		// TODO check this test case, maybe it should depend on a policy!! and report like UNREVOKED a signature made
		// before some x time (specified in the policy) the certificate got revoked by a malicious reaons
		///////////////////////////////////////
		
		// certificate revoked right now
		generatedCRL = generateCRL(trustedIntermediateCAKey, trustedIntermediateCACert, BigInteger.ONE, new Date(), CRLReason.keyCompromise);
		
		// signed 5 minutes ago
		signedPDFByteArray = generateSignedPDF(endEntityPrivateKey, new Certificate[] { trustedEndEntityCertificate,
				trustedIntermediateCACert }, DateTime.now().minusMinutes(5).toGregorianCalendar());
		intermediateCRLHttpServer = publishCRLWithHttpServer(generatedCRL, "/intermediatecrl", 10001);
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.REVOKED));
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		
		/////////////////////////////////////
		// if signature was made before certificate got revoked by a 'benign' reason code OK status, see:
		// http://csrc.nist.gov/nissc/1998/proceedings/paperG2.pdf
		////////////////////////////////////////
		intermediateCRLHttpServer.stop(1);
		// certificate revoked right now
		generatedCRL = generateCRL(trustedIntermediateCAKey, trustedIntermediateCACert, BigInteger.ONE, new Date(),
				CRLReason.cessationOfOperation);
		intermediateCRLHttpServer = publishCRLWithHttpServer(generatedCRL, "/intermediatecrl", 10001);
		// signedPDFByteArray signed 5 minutes ago
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.OK));
		
		///////////////////////////////////////
		// if dist point doesn't exist return unknown status but valid signature
		//////////////////////////////////////
		X509Certificate endEntityCertificateWithoutCDPExts = generateEndEntityX509Certificate(BigInteger.ONE, "CN=subject",
				endEntityPublicKey, trustedIntermediateCACert, trustedIntermediateCAKey, false, false);
		signedPDFByteArray = generateSignedPDF(endEntityPrivateKey, new Certificate[] { endEntityCertificateWithoutCDPExts,
				trustedIntermediateCACert });
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.UNKNOWN));
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		
		///////////////////////////////////////////////////////////////////////////
		// if downloaded CRL is outdated and certificate revoked, valid signature but UNKNOWN status, in these
		// situations
		// where validity haven't been checked yet, put UNKNOWN status for validity
		// TODO study the following, look for further documentation, maybe revocation status should be maintained,
		// except for 'certificateHold' revocation reason, see RFC 5280 5.2.4: The status of a certificate is considered
		// to have changed if it is revoked (for any revocation reason, including certificateHold), if it is released
		// from hold, or if its revocation reason changes.
		///////////////////////////////////////////////////////////////////////////
		intermediateCRLHttpServer.stop(1);
		DateTime twentyMinutesAgo = new DateTime().minusMinutes(20);
		generatedCRL = generateCRL(trustedIntermediateCAKey, trustedIntermediateCACert, twentyMinutesAgo.toDate(), twentyMinutesAgo
                .plusMinutes(10).toDate(), BigInteger.ONE, twentyMinutesAgo.plusMinutes(1).toDate(), CRLReason.keyCompromise);
		intermediateCRLHttpServer = publishCRLWithHttpServer(generatedCRL, "/intermediatecrl", 10001);
		signedPDFByteArray = generateSignedPDF(endEntityPrivateKey, new Certificate[]{trustedEndEntityCertificate,
                trustedIntermediateCACert});
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.UNKNOWN));
		assertThat(signatureResult.getSignerVerificationStatus().getValidityStatus().getStatus(),
				is(SignatureResult.ValidityStatus.Status.UNKNOWN));
		// TODO assert message indicates something about the fact that the CRL is outdated, even if later this test
		// case is modified to report the REVOKED status (except for 'certificateHold' for which UNKNOWN should be
		// reported), user should be aware that he is hitting an outdated CRL... furthermore it should be logged
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		
		///////////////////////////////////////////////////////////////////////////
		// if downloaded CRL is outdated with respect to the date in the signed pdf and certificate not revoked in
		// that CRL, valid signature but UNKNOWN
		// status.
		///////////////////////////////////////////////////////////////////////////
		intermediateCRLHttpServer.stop(1);
		twentyMinutesAgo = new DateTime().minusMinutes(20);
		generatedCRL = generateCRL(trustedIntermediateCAKey, trustedIntermediateCACert, twentyMinutesAgo.toDate(), twentyMinutesAgo
				.plusMinutes(10).toDate(), null, null, -1); // no entry is revoked
		
		intermediateCRLHttpServer = publishCRLWithHttpServer(generatedCRL, "/intermediatecrl", 10001);
		signedPDFByteArray = generateSignedPDF(endEntityPrivateKey, new Certificate[] { trustedEndEntityCertificate,
				trustedIntermediateCACert });
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.UNKNOWN));
		// TODO assert message indicates something about the fact that the CRL is outdated
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		
		//////////////////////////////////////
		// date used for validation of status should be the purported date on signature (if there is no timestamp)
		//////////////////////////////////////
		// signature time: 20 minutes ago
		signedPDFByteArray = generateSignedPDF(endEntityPrivateKey, new Certificate[] { trustedEndEntityCertificate,
				trustedIntermediateCACert }, DateTime.now().minusMinutes(20).toGregorianCalendar());
		// certificate revoked 10 minus ago with benign crlReason
		generatedCRL = generateCRL(trustedIntermediateCAKey, trustedIntermediateCACert, BigInteger.ONE, new DateTime().minusMinutes(10)
				.toDate(), CRLReason.cessationOfOperation);
		intermediateCRLHttpServer.stop(1);
		intermediateCRLHttpServer = publishCRLWithHttpServer(generatedCRL, "/intermediatecrl", 10001);
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.OK));
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		
		//////////////////////////////////////
		//date used for validation of status should be the timestmap date on signature (if there is a valid
		// timestamp)
		///////////////////////////////////
		
		/////////////////////////////////
		// test it simulating a TSA with the clock with a little negative delay: purported signature date:
		// 10 minutes ago, timestamp date: 12 minutes ago, cert revoked 11 minutes ago
		///////////////////////////////////////////
		
		// TODO signature policy should validate delays between tsts and purported signature times
		intermediateCRLHttpServer.stop(1);
		// sign pdf and include tst
		signedPDFByteArray = generateSignedPDF(endEntityPrivateKey, new Certificate[] { trustedEndEntityCertificate,
				trustedIntermediateCACert }, DateTime.now().minusMinutes(10).toGregorianCalendar(), new MockTSAClient(DateTime.now()
				.minusMinutes(12).toDate()));
		
		// create crl.
		generatedCRL = generateCRL(trustedIntermediateCAKey, trustedIntermediateCACert, BigInteger.ONE, new DateTime().minusMinutes(11)
				.toDate(), CRLReason.cessationOfOperation);
		
		intermediateCRLHttpServer = publishCRLWithHttpServer(generatedCRL, "/intermediatecrl", 10001);
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.OK));
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		
		///////////////////////////////
		// test it simulating a TSA with the clock with a little negative delay: purported signature date:
		// 12 minutes ago, timestamp date: 10 minutes ago, cert revoked 11 minutes ago
		////////////////////////////////
		signedPDFByteArray = generateSignedPDF(endEntityPrivateKey, new Certificate[] { trustedEndEntityCertificate,
				trustedIntermediateCACert }, DateTime.now().minusMinutes(12).toGregorianCalendar(), new MockTSAClient(DateTime.now()
				.minusMinutes(10).toDate()));
		
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.REVOKED));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage(),
				startsWith("Signer certificate revoked at"));
		
		///////////////////////////////////////////////
		// validating a valid signature once the certificate has expired in relation to the downloaded CRL should
		// produce an unknown status and valid signature TODO until we get support for LTV
		//////////////////////////////////////////////////
		
		// generate a certificate that expired 20 minutes ago
		X509Certificate endEntityCertificateExpiredTwentyMinutesAgo = generateEndEntityX509Certificate(BigInteger.ONE, "CN=subject",
				endEntityPublicKey, trustedIntermediateCACert, trustedIntermediateCAKey, true, false,
				DateTime.now().minusHours(1).toDate(), twentyMinutesAgo.toDate());
		
		// generate a signed pdf from 30 minutes ago
		signedPDFByteArray = generateSignedPDF(endEntityPrivateKey, new Certificate[] { endEntityCertificateExpiredTwentyMinutesAgo,
				trustedIntermediateCACert }, DateTime.now().minusMinutes(30).toGregorianCalendar());
		
		generatedCRL = generateCRL(trustedIntermediateCAKey, trustedIntermediateCACert); // crl not revocating the
		intermediateCRLHttpServer.stop(1);
		intermediateCRLHttpServer = publishCRLWithHttpServer(generatedCRL, "/intermediatecrl", 10001);
		
		// validate the signature
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.UNKNOWN));
		// TODO later test we actually receive a message indicating the certificate has expired or something like
		// that so we can't find a appropiate CRL, maybe in the detailed message?
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getDetailedMessage());
		
		/////////////////////////////////////////////////////////////////////////////
		// validating a valid signature once the certificate has expired, but with a crl with thisUpdate previous than
		// the certificate expiration should produce a valid signature and OK status
		/////////////////////
		generatedCRL = generateCRL(trustedIntermediateCAKey, trustedIntermediateCACert, twentyMinutesAgo.minusMinutes(2).toDate(),
				twentyMinutesAgo.plusMinutes(2).toDate(), null, null, -1);
		intermediateCRLHttpServer.stop(1);
		intermediateCRLHttpServer = publishCRLWithHttpServer(generatedCRL, "/intermediatecrl", 10001);
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.OK));
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		
		///////////////////////////////////////////////////////////
		// if untrusted signer unknown revocation status but valid cryptographic signature
		// TODO check UNKNOWN status for any untrusted signer certificate with an appropiate message
		//////////////////////////////////////////////////////////
		X509Certificate untrustedEndEntityCertificate = generateEndEntityX509Certificate(BigInteger.ONE, "CN=subject", endEntityPublicKey,
				untrustedCACertificate, untrustedCAPrivateKey, true, false);
		
		signedPDFByteArray = generateSignedPDF(endEntityPrivateKey, new Certificate[] { untrustedEndEntityCertificate,
				untrustedCACertificate });
		generatedCRL = generateCRL(untrustedCAPrivateKey, untrustedCACertificate);
		intermediateCRLHttpServer.stop(1);
		intermediateCRLHttpServer = publishCRLWithHttpServer(generatedCRL, "/intermediatecrl", 10001);
		
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.UNKNOWN));
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		
		/////////////////////////////////////////////
		// signed PDF contains only the trusted signer certificate which is not connected to its trust anchor
		// because the PDF structure is missing the intermediate certificate
		/////////////////////////////////////////////
		signedPDFByteArray = generateSignedPDF(endEntityPrivateKey, new Certificate[] { trustedEndEntityCertificate });
		
		generatedCRL = generateCRL(trustedIntermediateCAKey, trustedIntermediateCACert);
		intermediateCRLHttpServer.stop(1);
		intermediateCRLHttpServer = publishCRLWithHttpServer(generatedCRL, "/intermediatecrl", 10001);
		
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.UNKNOWN));
		assertThat(signatureResult.getSignerVerificationStatus().getTrustStatus().getStatus(),
				is(SignatureResult.TrustStatus.Status.UNTRUSTED));
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		
		//////////////////////////////
		//end entity + inter where inter is not directly connected to any trust anchor: UNKNOWN status
		// TODO evaluate if this test case wouldn't be more appropiate in the test method for evaluating trust
		/////////////////////////////
		X509Certificate intermediateCADerivedFromTrustedIntermediate = generateIntermediateCertificate(trustedRootCAKey,
				trustedIntermediateCACert.getPublicKey(), new X500Principal("CN=unexistent parent CA"), new X500Principal(
						"CN=test trusted Intermediate CA"), "http://localhost:10003/unexistentcacrl");
		
		signedPDFByteArray = generateSignedPDF(endEntityPrivateKey, new Certificate[] { trustedEndEntityCertificate,
				intermediateCADerivedFromTrustedIntermediate });
		
		// currently unnecessary as our implementation transforms higher untrusted CA in a trust anchor for CPV
		// purposes, so, no CRL would be looked for for 'intermediateCADerivedFromTrustedIntermediate', furthermore no
		// check should be made because CRL checking should depend on TRUSTED trust status.
		X509CRL unexistentCACRL = generateCRL(trustedRootCAKey, new X500Principal("CN=unexistent parent CA"), new Date(), DateTime.now()
				.plusHours(1).toDate(), null, null, -1);
		HttpServer unexistentParentCAHttpServer = publishCRLWithHttpServer(unexistentCACRL, "/unexistentcacrl", 10003);
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.UNKNOWN));
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		
		unexistentParentCAHttpServer.stop(1);
		
		//////////////////////////////////////////
		// if the intermediate certificate is the revoked one the message status should indicate this
		//////////////////////////////////////////
		rootCRL = generateCRL(trustedRootCAKey, trustedRootCACert, BigInteger.ONE); // revoking the intermediate CA
		rootCRLHttpServer.stop(1);
		rootCRLHttpServer = publishCRLWithHttpServer(rootCRL, "/rootcrl", 10002);
		signedPDFByteArray = generateSignedPDF(endEntityPrivateKey, new Certificate[] { trustedEndEntityCertificate,
				trustedIntermediateCACert });
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.REVOKED));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage(),
				containsString("CN=test trusted Intermediate CA"));
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		
		///////////////////////
		// CRL not available for download for the intermediate certificate, UNKNOWN status
		//////////////////////
		rootCRLHttpServer.stop(1);
		signatureResult = pdfSignatureValidator.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.UNKNOWN));
		System.out.println(signatureResult.getSignerVerificationStatus().getRevocationStatus().getMessage());
		
		////////////////////////////////////
		// test with a revoked ICA being at the same time the trust anchor, it should get OK status
		///////////////////////////////////
		OKMITextPDFSignature pdfSignatureValidatorWithICAAsTrustAnchor = new OKMITextPDFSignature(
				Collections.singleton(trustedIntermediateCACert));
		rootCRLHttpServer = publishCRLWithHttpServer(rootCRL, "/rootcrl", 10002);
		signatureResult = pdfSignatureValidatorWithICAAsTrustAnchor.verify(signedPDFByteArray);
		assertThat(signatureResult.getCryptographicSignatureStatus().getStatus(),
				is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.OK));
		
	}
	
	/**
	 * @verifies verify time validity status at the time of signing for all certs in certification path
	 * @see OKMPDFSignature#verify(java.io.InputStream)
	 */
	@Test
	public void verify_shouldVerifyTimeValidityStatusAtTheTimeOfSigningForAllCertsInCertificationPath() throws Exception {
		
		/////////////////////
		// verify against signed pdf with untrusted certificate that expired 20 minutes ago, expect UNTRUSTED trust
		// status,
		///////////////////
		X509Certificate untrustedEndEntityCertificateExpiredTwentyMinutesAgo = generateEndEntityX509Certificate(BigInteger.ONE,
				"CN=subject", endEntityPublicKey, untrustedCACertificate, untrustedCAPrivateKey, true, false, DateTime.now().minusHours(1)
						.toDate(), DateTime.now().minusMinutes(20).toDate());
		
		// create pdf signed 10 minutes ago
		byte[] signedPdfOS = generateSignedPDF(endEntityPrivateKey, new Certificate[] {
				untrustedEndEntityCertificateExpiredTwentyMinutesAgo, untrustedCACertificate }, DateTime.now().minusMinutes(10)
				.toGregorianCalendar());

		// expect valid signature but invalid validityStatus and assert message
		SignatureResult verify = pdfSignatureValidator.verify(signedPdfOS);
		assertThat(verify.getCryptographicSignatureStatus().getStatus(), is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(verify.getSignerVerificationStatus().getValidityStatus().getStatus(), is(SignatureResult.ValidityStatus.Status.EXPIRED));
		assertThat(verify.getSignerVerificationStatus().getTrustStatus().getStatus(), is(SignatureResult.TrustStatus.Status.UNTRUSTED));
		assertThat(verify.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.UNKNOWN));
		
		assertThat(verify.getSignerVerificationStatus().getRevocationStatus().getMessage(),
				is(I18N.getString("unable.to.process.revocation.status.because.signer.is.not.trusted")));
		
		System.out.println(verify.getSignerVerificationStatus().getValidityStatus().getMessage());
		assertThat(
				verify.getSignerVerificationStatus().getValidityStatus().getMessage(),
				startWithNChars(
						I18N.getString("certificate.was.expired.at.the.time.of.signing.expired.at.and.signature.produced.at", "", ""), 20));
		
		///////////////////////////
		// create trusted certificate expired at the time of signing, expect the TRUSTED trust status, and the UNKNOWN
		// revocation status with an appropiate message
		// TODO test with the trusted certificate alone in the PDF
		////////////////////////
		X509CRL rootCRL = generateCRL(trustedRootCAKey, trustedRootCACert);
		rootCRLHttpServer = publishCRLWithHttpServer(rootCRL, "/rootcrl", 10002);
		X509CRL intermediateCRL = generateCRL(trustedIntermediateCAKey, trustedIntermediateCACert);
		intermediateCRLHttpServer = publishCRLWithHttpServer(intermediateCRL, "/intermediatecrl", 10001);
		
		X509Certificate trustedEndEntityCertificateExpiredTwentyMinutesAgo = generateEndEntityX509Certificate(BigInteger.ONE, "CN=subject",
				endEntityPublicKey, trustedIntermediateCACert, trustedIntermediateCAKey, true, false,
				DateTime.now().minusHours(1).toDate(), DateTime.now().minusMinutes(20).toDate());
		
		// create pdf signed 10 minutes ago
		// TODO test what happens if we have a PDF including the entire trusted cert path up to OpenKM Root CA
		signedPdfOS = generateSignedPDF(endEntityPrivateKey, new Certificate[] { trustedEndEntityCertificateExpiredTwentyMinutesAgo,
				trustedIntermediateCACert }, DateTime.now().minusMinutes(10).toGregorianCalendar());

		// expect valid signature but invalid validityStatus and assert message
		verify = pdfSignatureValidator.verify(signedPdfOS);
		assertThat(verify.getCryptographicSignatureStatus().getStatus(), is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(verify.getSignerVerificationStatus().getValidityStatus().getStatus(), is(SignatureResult.ValidityStatus.Status.EXPIRED));
		assertThat(verify.getSignerVerificationStatus().getTrustStatus().getStatus(), is(SignatureResult.TrustStatus.Status.TRUSTED));
		
		// expect UNKNOWN revocation status, see the order in RFC 5180, sec 6.1.3. (a) and the last part of that
		// section
		// "If any of steps (a), (b), (c), or (f) fails, the procedure terminates, returning a failure indication and an appropriate reason."
		// conclusion is that revocation checking is after certificate validity
		System.out.println(verify.getSignerVerificationStatus().getRevocationStatus().getMessage());
		assertThat(verify.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.UNKNOWN));
		
		System.out.println(verify.getSignerVerificationStatus().getValidityStatus().getMessage());
		assertThat(
				verify.getSignerVerificationStatus().getValidityStatus().getMessage(),
				startWithNChars(
						I18N.getString("certificate.was.expired.at.the.time.of.signing.expired.at.and.signature.produced.at", "", ""), 20));
		
		//////////////////////////////////
		// validate against valid untrusted certificate at the time of signing
		///////////////////////////
		
		// pdf signed 30 minutes ago
		signedPdfOS = generateSignedPDF(endEntityPrivateKey, new Certificate[] { untrustedEndEntityCertificateExpiredTwentyMinutesAgo,
				untrustedCACertificate }, DateTime.now().minusMinutes(30).toGregorianCalendar());
		
		verify = pdfSignatureValidator.verify(signedPdfOS);
		assertThat(verify.getCryptographicSignatureStatus().getStatus(), is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(verify.getSignerVerificationStatus().getValidityStatus().getStatus(), is(SignatureResult.ValidityStatus.Status.VALID));
		System.out.println(verify.getSignerVerificationStatus().getValidityStatus().getMessage());
		assertThat(verify.getSignerVerificationStatus().getValidityStatus().getMessage(),
				startsWith(I18N.getString("certificate.was.valid.at.the.time.of.signing")));
		
		//////////////////////
		// test what happens when it is not the end entity certificate the expired one, message is important to point
		// to invalid certificate!
		//////////////////////
		
		KeyPair invalidIntermediateKeyPair = generateRSAKeyPair();
		X509V3CertificateGenerator x509V3CertificateGenerator = new X509V3CertificateGenerator();
		x509V3CertificateGenerator.setIssuerDN(untrustedCACertificate.getSubjectX500Principal());
		x509V3CertificateGenerator.setSerialNumber(BigInteger.ONE);
		// valid from yesterday
		x509V3CertificateGenerator.setNotBefore(DateTime.now().minusDays(2).toDate());
		x509V3CertificateGenerator.setNotAfter(DateTime.now().minusDays(1).toDate());
		x509V3CertificateGenerator.setSubjectDN(new X500Principal("CN=invalid intermediate"));
		x509V3CertificateGenerator.setPublicKey(invalidIntermediateKeyPair.getPublic());
		x509V3CertificateGenerator.setSignatureAlgorithm("SHA1WITHRSA");
		X509Certificate invalidIntermediate = x509V3CertificateGenerator.generate(untrustedCAPrivateKey);
		
		X509Certificate validEndEntityCertificateSignedByInvalidIntermediate = generateEndEntityX509Certificate(BigInteger.ONE,
				"CN=subject", endEntityPublicKey, invalidIntermediate, invalidIntermediateKeyPair.getPrivate(), true, false, DateTime.now()
						.minusHours(1).toDate(), DateTime.now().plusHours(1).toDate());
		
		signedPdfOS = generateSignedPDF(endEntityPrivateKey, new Certificate[] { validEndEntityCertificateSignedByInvalidIntermediate,
				invalidIntermediate, untrustedCACertificate });
		
		verify = pdfSignatureValidator.verify(signedPdfOS);
		assertThat(verify.getCryptographicSignatureStatus().getStatus(), is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(verify.getSignerVerificationStatus().getValidityStatus().getStatus(), is(SignatureResult.ValidityStatus.Status.EXPIRED));
		System.out.println(verify.getSignerVerificationStatus().getValidityStatus().getMessage());
		assertThat(verify.getSignerVerificationStatus().getValidityStatus().getMessage(), containsString("CN=invalid intermediate"));
		
		/////////////////////////////////////////
		////////// / test what happens if it is the root/anchor the expired one: anchor validity is ignored
		///////////////////////////////////////////////////
		
		// create expired (at the time of signing) v1 certificate for trust anchor
		KeyPair expiredRootKeyPair = generateRSAKeyPair();
		X509V1CertificateGenerator x509V1CertificateGenerator = new X509V1CertificateGenerator();
		x509V1CertificateGenerator.setSerialNumber(BigInteger.ONE);
		X500Principal expiredTrustAnchorDN = new X500Principal("CN=expired trust anchor");
		x509V1CertificateGenerator.setIssuerDN(expiredTrustAnchorDN);
		x509V1CertificateGenerator.setSubjectDN(expiredTrustAnchorDN);
		x509V1CertificateGenerator.setNotBefore(DateTime.now().minusDays(2).toDate());
		x509V1CertificateGenerator.setNotAfter(DateTime.now().minusDays(1).toDate());
		x509V1CertificateGenerator.setPublicKey(expiredRootKeyPair.getPublic());
		x509V1CertificateGenerator.setSignatureAlgorithm("sha1withrsa");
		X509Certificate expiredRoot = x509V1CertificateGenerator.generate(expiredRootKeyPair.getPrivate());
		
		// create end entity certificate not expired at the time of signing (certificate validity is not
		// included in its CA validity period, see if this is verified in PKIX CPV, if not there... where?)
		X509Certificate validEndEntityFromExpiredRoot = generateEndEntityX509Certificate(BigInteger.ONE, "CN=subject", endEntityPublicKey,
				expiredRoot, expiredRootKeyPair.getPrivate(), false, false, DateTime.now().minusHours(1).toDate(), DateTime.now()
						.plusHours(1).toDate());
		
		signedPdfOS = generateSignedPDF(endEntityPrivateKey, new Certificate[] { validEndEntityFromExpiredRoot });
		
		OKMITextPDFSignature foo = new OKMITextPDFSignature(Collections.singleton(expiredRoot));
		verify = foo.verify(signedPdfOS);
		assertThat(verify.getCryptographicSignatureStatus().getStatus(), is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		
		/////////////////////////
		// test for not yet valid certificates (end entity)
		//////
		X509Certificate trustedEndEntityCertificateNotYetValid = generateEndEntityX509Certificate(BigInteger.ONE, "CN=subject",
				endEntityPublicKey, trustedIntermediateCACert, trustedIntermediateCAKey, true, false, DateTime.now().plusHours(1).toDate(),
				DateTime.now().plusHours(2).toDate()); // valid in one hour
		
		signedPdfOS = generateSignedPDF(endEntityPrivateKey, new Certificate[] { trustedEndEntityCertificateNotYetValid,
				trustedIntermediateCACert });
		verify = pdfSignatureValidator.verify(signedPdfOS);
		assertThat(verify.getCryptographicSignatureStatus().getStatus(), is(SignatureResult.CryptographicSignatureStatus.Status.VALID));
		assertThat(verify.getSignerVerificationStatus().getValidityStatus().getStatus(),
				is(SignatureResult.ValidityStatus.Status.NOT_YET_VALID));
		
		System.out.println(verify.getSignerVerificationStatus().getValidityStatus().getMessage());
		
		//
		// TODO test for not yet valid certificates (CAs and trust anchors)
		//////
		
		//
		// TODO test that validity status is independent from revocation status: UNKNOWN, REVOKED, OK, whatever
		// create following tests: expired + revoved, revoked + valid, expired + unknown
		//
		
	}
	
	private Matcher<String> startWithNChars(String prefix, int numberOfChars) {
		String str = prefix;
		str = str.substring(0, numberOfChars);
		return startsWith(str);
	}
	
	private byte[] generateSignedPDF(PrivateKey endEntityPrivateKey, Certificate[] certPath) throws IOException, DocumentException,
            GeneralSecurityException {
		return generateSignedPDF(endEntityPrivateKey, certPath, Calendar.getInstance(), null);
	}
	
	private byte[] generateSignedPDF(PrivateKey endEntityPrivateKey, Certificate[] certPath, Calendar signDate) throws IOException,
            DocumentException, GeneralSecurityException {
		return generateSignedPDF(endEntityPrivateKey, certPath, signDate, null);
	}

    /**
     * See <code>http://svn.code.sf.net/p/itext/code/tutorial/signatures/src/main/java/signatures/chapter2/C2_01_SignHelloWorld.java r6063</code>.
     */
	// TODO modify signature to only require the date of the timestamp (instead of a TSAClient) if it is required to
	// be generated
	private byte[] generateSignedPDF(PrivateKey endEntityPrivateKey, Certificate[] certPath, Calendar signDate, TSAClient tsaClient)
            throws IOException, DocumentException, GeneralSecurityException {
		
		PdfReader reader = new PdfReader(getClass().getResourceAsStream("/unsigned_pdf.pdf"), null);
		
		// Third param is PDF revision (char).
		ByteArrayOutputStream fout = new ByteArrayOutputStream();
		PdfStamper stp = PdfStamper.createSignature(reader, fout, '\0', null, true);
		PdfSignatureAppearance sap = stp.getSignatureAppearance();
		
		sap.setSignDate(signDate);

        MakeSignature.signDetached(sap, new BouncyCastleDigest(), new PrivateKeySignature(endEntityPrivateKey, DigestAlgorithms.SHA1, "BC"), certPath, null, null, tsaClient, 0, MakeSignature.CryptoStandard.CMS);

        return fout.toByteArray();
	}
	
	private X509Certificate generateEndEntityX509Certificate(BigInteger serialNumber, String subjectDN, PublicKey endEntityPublicKey,
			X509Certificate caCertificate, PrivateKey caPrivateKey, boolean includecRLDistPoints, boolean includeTimeStampingEKU)
			throws CertificateEncodingException, NoSuchAlgorithmException, SignatureException, InvalidKeyException {
		
		Date notBefore = new DateTime().minusHours(1).toDate();
		Date notAfter = new DateTime().plusHours(1).toDate();
		
		return generateEndEntityX509Certificate(serialNumber, subjectDN, endEntityPublicKey, caCertificate, caPrivateKey,
				includecRLDistPoints, includeTimeStampingEKU, notBefore, notAfter);
	}
	
	private X509Certificate generateEndEntityX509Certificate(BigInteger serialNumber, String subjectDN, PublicKey endEntityPublicKey,
			X509Certificate caCertificate, PrivateKey caPrivateKey, boolean includecRLDistPoints, boolean includeTimeStampingEKU,
			Date notBefore, Date notAfter) throws CertificateEncodingException, NoSuchAlgorithmException, SignatureException,
			InvalidKeyException {
		X509V3CertificateGenerator x509V3CertificateGenerator = new X509V3CertificateGenerator();
		x509V3CertificateGenerator.setIssuerDN(caCertificate.getSubjectX500Principal());
		x509V3CertificateGenerator.setSerialNumber(serialNumber);
		x509V3CertificateGenerator.setNotBefore(notBefore);
		x509V3CertificateGenerator.setNotAfter(notAfter);
		x509V3CertificateGenerator.setSubjectDN(new X500Principal(subjectDN));
		x509V3CertificateGenerator.setPublicKey(endEntityPublicKey);
		x509V3CertificateGenerator.setSignatureAlgorithm("SHA1WITHRSA");
		if (includecRLDistPoints) { // create end user certificate with crl dist point
			x509V3CertificateGenerator.addExtension(
					X509Extension.cRLDistributionPoints,
					false,
					new CRLDistPoint(
							new DistributionPoint[] { new DistributionPoint(new DistributionPointName(new GeneralNames(GeneralName
									.getInstance(new DERTaggedObject(6, new DERIA5String("http://localhost:10001/intermediatecrl"))))),
									null, null) }));
		}
		if (includeTimeStampingEKU) {
			x509V3CertificateGenerator.addExtension(X509Extension.extendedKeyUsage, true, new ExtendedKeyUsage(
					KeyPurposeId.id_kp_timeStamping));
		}
		
		return x509V3CertificateGenerator.generate(caPrivateKey);
	}
	
	// TODO encapsulate the creation and destroy of the HttpServers in an smarter way
	private HttpServer publishCRLWithHttpServer(final X509CRL crlForEndEntity, String path, int port) throws IOException {
		
		// TODO evaluate to check if it is already started and stop it before beginning
		
		// publish crl with http server
		// TODO evaluate to create 'HttpServer' in setUp method and make it a field
		HttpServer httpServer = HttpServer.create(new InetSocketAddress(port), 0);
		httpServer.createContext(path, new HttpHandler() {
			@Override
			public void handle(HttpExchange exchange) throws IOException {
				byte[] encoded = new byte[0];
				try {
					encoded = crlForEndEntity.getEncoded();
				} catch (CRLException e) {
					throw new RuntimeException(e);
				}
				exchange.sendResponseHeaders(200, encoded.length);
				OutputStream os = exchange.getResponseBody();
				os.write(encoded);
				os.close();
			}
		});
		
		httpServer.setExecutor(null);
		httpServer.start();
		// FIXME when stop isn't being called (e.g. when test didn't complete succesfully), subsecuent execution
		// yields: java.net.BindException: Address already in use: bind, we have to find the way: a 'destructor'?? how
		// does it get implemented in java? maybe with a tearDown method? it would work if the
		// 'intermediateCRLHttpServer' would be a
		// field, TODO see the set up method
		return httpServer;
	}
	
	private static X509CRL generateCRL(PrivateKey caPrivate, X509Certificate caCert, BigInteger revokedSerialNumber)
			throws CertificateParsingException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, CRLException,
			SignatureException {
		
		return generateCRL(caPrivate, caCert, revokedSerialNumber, new Date(), CRLReason.unspecified);
	}
	
	private static X509CRL generateCRL(PrivateKey caPrivateKey, X509Certificate caCertificate) throws CRLException,
			NoSuchProviderException, NoSuchAlgorithmException, SignatureException, InvalidKeyException {
		return generateCRL(caPrivateKey, caCertificate, null, null, -1);
	}
	
	private static X509CRL generateCRL(PrivateKey caPrivKey, X509Certificate caCert, BigInteger revokedSerialNumber, Date revocationDate,
			int crlReason) throws CRLException, NoSuchProviderException, NoSuchAlgorithmException, SignatureException, InvalidKeyException {
		
		Date thisUpdate = new Date();
		Date nextUpdate = new Date(thisUpdate.getTime() + 60 * 1000); // 60 seconds after
		
		return generateCRL(caPrivKey, caCert, thisUpdate, nextUpdate, revokedSerialNumber, revocationDate, crlReason);
	}
	
	// TODO modify signature to support in a nicer way CRL generation without any entry being revoked
	private static X509CRL generateCRL(PrivateKey aPrivate, X509Certificate x509Certificate, Date thisUpdate, Date nextUpdate,
			BigInteger revokedSerialNumber, Date revocationDate, int crlReason) throws CRLException, NoSuchProviderException,
			NoSuchAlgorithmException, SignatureException, InvalidKeyException {
		return generateCRL(aPrivate, x509Certificate.getSubjectX500Principal(), thisUpdate, nextUpdate, revokedSerialNumber,
				revocationDate, crlReason);
	}
	
	private static X509CRL generateCRL(PrivateKey caPrivateKey, X500Principal caSubjectDN, Date thisUpdate, Date nextUpdate,
			BigInteger revokedSerialNumber, Date revocationDate, int crlReason) throws CRLException, NoSuchProviderException,
			NoSuchAlgorithmException, SignatureException, InvalidKeyException {
		X509V2CRLGenerator x509V2CRLGenerator = new X509V2CRLGenerator();
		x509V2CRLGenerator.setIssuerDN(caSubjectDN);
		
		x509V2CRLGenerator.setThisUpdate(thisUpdate);
		x509V2CRLGenerator.setNextUpdate(nextUpdate);
		x509V2CRLGenerator.setSignatureAlgorithm("sha256withrsaencryption");
		
		if (revokedSerialNumber != null) {
			x509V2CRLGenerator.addCRLEntry(revokedSerialNumber, revocationDate, crlReason);
		}
		
		// x509V2CRLGenerator.addExtension(X509Extensions.AuthorityKeyIdentifier, false, new
		// AuthorityKeyIdentifierStructure(caCert));
		x509V2CRLGenerator.addExtension(X509Extensions.CRLNumber, false, new CRLNumber(BigInteger.ONE));
		
		return x509V2CRLGenerator.generate(caPrivateKey, "BC");
	}
	
	private KeyPair generateRSAKeyPair() throws NoSuchAlgorithmException, NoSuchProviderException {
		KeyPairGenerator bc = KeyPairGenerator.getInstance("RSA", "BC");
		bc.initialize(512);
		return bc.generateKeyPair();
	}
	
	/**
	 * @verifies return null if no signature is found
	 * @see OKMPDFSignature#verify(java.io.InputStream)
	 */
	@Test
	public void verify_shouldReturnNullIfNoSignatureIsFound() throws Exception {
		assertThat(pdfSignatureValidator.verify(getClass().getResourceAsStream("/unsigned_pdf.pdf")), is(nullValue()));
	}
	
	/**
	 * @verifies verify signer certificate trust status
	 * @see OKMPDFSignature#verify(java.io.InputStream)
	 */
	@Test
	public void verify_shouldVerifySignerCertificateTrustStatus() throws Exception {
		
		///////////////////////
		// verify signature with untrusted certificate
		///////////////////////
		
		X509Certificate untrustedEndEntityCertificate = generateEndEntityX509Certificate(BigInteger.ONE, "CN=subject", endEntityPublicKey,
				untrustedCACertificate, untrustedCAPrivateKey, true, false);
		
		byte[] signedPdfBA = generateSignedPDF(endEntityPrivateKey, new Certificate[] { untrustedEndEntityCertificate,
				untrustedCACertificate });
		SignatureResult signatureResult = pdfSignatureValidator.verify(signedPdfBA);
		assertThat(signatureResult.getSignerVerificationStatus().getTrustStatus().getStatus(),
				is(SignatureResult.TrustStatus.Status.UNTRUSTED));
		assertThat(signatureResult.getSignerVerificationStatus().getTrustStatus().getMessage(),
				is(I18N.getString("signer.certificate.is.not.trusted")));
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.UNKNOWN));
		
		/////////////////////
		// verify signature with trusted certificate
		//////////////////////
		
		// generate certificate from trusted
		X509Certificate trustedEntityCertificate = generateEndEntityX509Certificate(BigInteger.ONE, "CN=subject", endEntityPublicKey,
				trustedIntermediateCACert, trustedIntermediateCAKey, true, false);
		
		signedPdfBA = generateSignedPDF(endEntityPrivateKey, new Certificate[] { trustedEntityCertificate, trustedIntermediateCACert });
		signatureResult = pdfSignatureValidator.verify(signedPdfBA);
		assertThat(signatureResult.getSignerVerificationStatus().getTrustStatus().getStatus(),
				is(SignatureResult.TrustStatus.Status.TRUSTED));
		assertThat(signatureResult.getSignerVerificationStatus().getTrustStatus().getMessage(),
				is(I18N.getString("signer.certificate.is.trusted")));
		
		// TODO if the PDF contains the certification path including the trust anchor should report TRUSTED
		signedPdfBA = generateSignedPDF(endEntityPrivateKey, new Certificate[] { trustedEntityCertificate, trustedIntermediateCACert,
				trustedRootCACert });
		signatureResult = pdfSignatureValidator.verify(signedPdfBA);
		assertThat(signatureResult.getSignerVerificationStatus().getTrustStatus().getStatus(),
				is(SignatureResult.TrustStatus.Status.TRUSTED));
		assertThat(signatureResult.getSignerVerificationStatus().getTrustStatus().getMessage(),
				is(I18N.getString("signer.certificate.is.trusted")));
		
		// TODO self signed signer certs not in trust store should report UNTRUSTED status
		
		// TODO self signed signer certs in trust store should report TRUSTED status
		
		// TODO not self signed signer certs not in trust store should report UNTRUSTED status
		
		// TODO not self signed signer certs in trust store should report TRUSTED status, possible issues around it
		// in BC 1.46 CPV implementation. see if the following has any relationship RFC 5280 sec. 6.1 When the trust
		// anchor is provided in the form of a self-signed certificate, this self-signed certificate is not included as
		// part of the prospective certification path.
		
	}
	
	/**
	 * @verifies return support multiple signatures and relate them
	 * @see OKMPDFSignature#verify(java.io.InputStream)
	 */
	@Test
	public void verify_shouldReturnSupportMultipleSignaturesAndRelateThem() throws Exception {
		printSignaturesMap(pdfSignatureValidator.verify(OKMPDFSignatureTest.class
				.getResourceAsStream("/adbe_pkcs7_detached_sha1_multiple_valid_signatures_trusted_signers_3.pdf")));
		printSignaturesMap(pdfSignatureValidator.verify(OKMPDFSignatureTest.class
				.getResourceAsStream("/adbe_pkcs7_detached_sha256_multiple_valid_signatures_trusted_signers_1.pdf")));
		printSignaturesMap(pdfSignatureValidator.verify(OKMPDFSignatureTest.class
				.getResourceAsStream("/adbe_pkcs7_detached_sha256_multiple_valid_signatures_trusted_signers_2.pdf")));
		// TODO put some assert, at least to count signatures
	}
	
	private void printSignaturesMap(SignatureResult signatureResult) {
		System.out.println("Signatures map");
		int level = 1;
		do {
			System.out.println(StringUtils.repeat("-", level) + signatureResult);
			signatureResult = signatureResult.getCounterSignature();
			level++;
		} while (signatureResult != null);
	}
	
	/**
	 * @verifies provide UNKNOWN status for all CPV statuses if cryptographic signature is invalid, and do not provide
	 *           the signer certificate or any other signature data
	 * @see OKMPDFSignature#verify(java.io.InputStream)
	 */
	@Test
	public void verify_shouldProvideUNKNOWNStatusForAllCPVStatusesIfCryptographicSignatureIsInvalidAndDoNotProvideTheSignerCertificateOrAnyOtherSignatureData()
			throws Exception {
		// test for corrupted signatures
		SignatureResult signatureResult = verifyClasspathResource("/adbe_pkcs7_detached_sha256_corrupted_signature_untrusted_signer.pdf");
		assertSignatureResultIsInUnknownState(signatureResult);
		
		// test for invalid signatures
		signatureResult = verifyClasspathResource("/adbe_pkcs7_detached_sha256_invalid_signature_untrusted_signer.pdf");
		assertSignatureResultIsInUnknownState(signatureResult);
		
	}
	
	private void assertSignatureResultIsInUnknownState(SignatureResult signatureResult) {
		assertThat(signatureResult.getSignerVerificationStatus().getRevocationStatus().getStatus(),
				is(SignatureResult.RevocationStatus.Status.UNKNOWN));
		assertThat(signatureResult.getSignerVerificationStatus().getTrustStatus().getStatus(),
				is(SignatureResult.TrustStatus.Status.UNKNOWN));
		assertThat(signatureResult.getSignerVerificationStatus().getValidityStatus().getStatus(),
				is(SignatureResult.ValidityStatus.Status.UNKNOWN));
		assertThat(signatureResult.getSigningLocation(), is(nullValue()));
		assertThat(signatureResult.getSignerCertificate(), is(nullValue()));
		assertThat(signatureResult.getSigningReason(), is(nullValue()));
		assertThat(signatureResult.getSigningTime(), is(nullValue()));
		
		assertThat(signatureResult.getSignedVersionDocument(), is(nullValue()));
		
	}
	
	// TODO determine if this is ok the 'Mock' prefix for this class
	private class MockTSAClient implements TSAClient {
		
		private MockTSAClient(Date timestampDate) {
			this.timestampDate = timestampDate;
		}
		
		private Date timestampDate;
		
		@Override
		public int getTokenSizeEstimate() {
			throw new UnsupportedOperationException();
		}

        @Override
        public MessageDigest getMessageDigest() throws GeneralSecurityException {
            return MessageDigest.getInstance("SHA-1");
        }

		@Override
		public byte[] getTimeStampToken(byte[] imprint) throws Exception {
			
			KeyPair keyPair = generateRSAKeyPair();
			X509Certificate tsaCertificate = generateEndEntityX509Certificate(BigInteger.valueOf(3), "CN=tsa", keyPair.getPublic(),
					untrustedCACertificate, untrustedCAPrivateKey, false, true);
			TimeStampTokenGenerator tsTokenGen = new TimeStampTokenGenerator(new JcaSimpleSignerInfoGeneratorBuilder().build("SHA1withRSA",
					keyPair.getPrivate(), tsaCertificate), new ASN1ObjectIdentifier("1.2"));
			tsTokenGen.addCertificates(new JcaCertStore(Collections.singleton(tsaCertificate)));
			
			TimeStampResponseGenerator timeStampResponseGenerator = new TimeStampResponseGenerator(tsTokenGen, TSPAlgorithms.ALLOWED);
			
			TimeStampRequestGenerator reqGen = new TimeStampRequestGenerator();
			reqGen.setCertReq(true);
			
			TimeStampRequest request = reqGen.generate(TSPAlgorithms.SHA1, imprint, BigInteger.valueOf(100));
			
			// create tst response
			// TODO determine if it is needed to provide the serial number as argument
			TimeStampResponse generate = timeStampResponseGenerator.generate(request, BigInteger.valueOf(4), timestampDate);
			
			return generate.getTimeStampToken().getEncoded();
		}
	}
	
	// TODO evaluate to move the following methods to another test class, maybe one with a main(String...) method as
	// they are not really unit tests
	
	/**
	 * Verifies a valid signature on a PDF file.
	 * 
	 * @verifies allow to verify a valid signature with subfilter adbe.pkcs7.detached
	 * @see OKMPDFSignature#verify(java.io.InputStream)
	 */
	@Test
	public void verify_shouldAllowToVerifyAValidSignatureWithSubfilterAdbepkcs7detached() throws Exception {
		verifyFile("/adbe_pkcs7_detached_sha256_valid_signature_untrusted_signer.pdf");
	}
	
	/**
	 * Verifies a wrong signature, intentionally altered.
	 * 
	 * @verifies allow to verify a corrupt signature with subfilter adbe.pkcs7.detached
	 * @see OKMPDFSignature#verify(java.io.InputStream)
	 */
	@Test
	public void verify_shouldAllowToVerifyACorruptSignatureWithSubfilterAdbepkcs7detached() throws Exception {
		// verify wrong signature and display results
		verifyFile("/adbe_pkcs7_detached_sha256_corrupted_signature_untrusted_signer.pdf");
		
	}
	
	/**
	 * @verifies allow to verify a invalid signature with subfilter adbe.pkcs7.detached
	 * @see OKMPDFSignature#verify(java.io.InputStream)
	 */
	@Test
	public void verify_shouldAllowToVerifyAInvalidSignatureWithSubfilterAdbepkcs7detached() throws Exception {
		verifyFile("/adbe_pkcs7_detached_sha256_invalid_signature_untrusted_signer.pdf");
		
	}
	
}
