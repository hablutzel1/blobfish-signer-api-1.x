/**
 * OpenKM, Open Document Management System (http://www.openkm.com)
 * Copyright (c) 2006-2013 Paco Avila & Josep Llort
 * 
 * No bytes were intentionally harmed during the development of this application.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package pe.blobfish.signature;

import java.io.InputStream;

// TODO evaluate to create a Factory to create OKMPDFSignature objects.
// TODO check: maybe there is no real need for an interface here!.
public interface OKMPDFSignature {
	
	/**
	 * @param input the PDF file represented as a byte array
	 * @return
	 * @see OKMPDFSignature#verify(InputStream)
	 */
	SignatureResult verify(byte[] input) throws OKMPDFSignatureException;
	
	/**
	 * @param is an InputStream for the PDF file
	 * @return a SignatureResult object for the first (and possibly only) PDF signature or null if no signature is found
     * @throws OKMPDFSignatureException for any general problem not related to PDF signatures verification
	 * @should allow to verify a valid signature with subfilter adbe.pkcs7.detached
	 * @should allow to verify a corrupt signature with subfilter adbe.pkcs7.detached
	 * @should allow to verify a invalid signature with subfilter adbe.pkcs7.detached
	 * @should verify signer certificate trust status
	 * @should verify revocation status at the time of signing for all certs in certification path
	 * @should verify time validity status at the time of signing for all certs in certification path
	 * @should provide UNKNOWN status for all CPV statuses if cryptographic signature is invalid, and do not provide the
	 *         signer certificate or any other signature data
	 * @should return support multiple signatures and relate them
	 * @should return null if no signature is found
	 * @should provide document snapshot for each signature result with VALID cryptographic status only, use files with
	 *         major changes over revisions to demostrate the need for this
	 * @should return a invalid cryptographic signature if there is any unmanaged problem in the CPV like unexistent
	 *         basic constraints in CA certificates
	 */
	SignatureResult verify(InputStream is) throws OKMPDFSignatureException;
}
