/**
 * OpenKM, Open Document Management System (http://www.openkm.com)
 * Copyright (c) 2006-2013 Paco Avila & Josep Llort
 * 
 * No bytes were intentionally harmed during the development of this application.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package pe.blobfish.signature;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public final class I18N {

    private static final String RESOURCE_BUNDLE_BASE_PACKAGE = "pe/blobfish/signature/";
    private static final String ACTUAL_BUNDLE_NAME = "messages";

    // TODO determine if this is expensive to call java.util.ResourceBundle.getBundle(...) as it will be being called
    // over and over again, if expensive cache each language, for each user with every call or for every user? and lazy
    // load them
    private static ThreadLocal<ResourceBundle> resourceBundle = new ThreadLocal<ResourceBundle>(){
        @Override
        protected ResourceBundle initialValue() {
            return ResourceBundle.getBundle(RESOURCE_BUNDLE_BASE_PACKAGE + ACTUAL_BUNDLE_NAME, new DefaultBundleControl());
        }
    };

    /**
     * <p>
     * Sets the locale to be used to i18n 'keys' by the current thread of execution
     * </p>
     * @param locale
     */
	public static void setLocale(Locale locale) {
        resourceBundle.set(ResourceBundle.getBundle(RESOURCE_BUNDLE_BASE_PACKAGE + ACTUAL_BUNDLE_NAME, locale, new DefaultBundleControl()));
	}

	static String getString(String key) {
		return getString(key, new String[0]);
	}

	static String getString(String key, String... parameter) {
		
        /// TODO look if this is currently failing if it doesn't find a key in the bundle, is it the expected behaviour? or should it fall back to the default bundle?
		try {
			return MessageFormat.format(resourceBundle.get().getString(key), parameter);
		} catch (MissingResourceException e) {
			return key;
		}
	}



    /**
     * If the desired locale isn't found it falls back to messages.properties, not to the current JVM default locale
     *
     */
    // TODO test if this is allowing to fall back at least to the language part of the locale definition
    private static class DefaultBundleControl extends ResourceBundle.Control {
        @Override
        public Locale getFallbackLocale(String baseName, Locale locale) {
            return null;
        }
    }

}
