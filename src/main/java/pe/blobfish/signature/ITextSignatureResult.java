/**
 * OpenKM, Open Document Management System (http://www.openkm.com)
 * Copyright (c) 2006-2013 Paco Avila & Josep Llort
 * 
 * No bytes were intentionally harmed during the development of this application.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package pe.blobfish.signature;

import java.io.InputStream;
import java.util.Date;

import com.itextpdf.text.pdf.security.PdfPKCS7;
import org.apache.log4j.Logger;


/**
 * iText specific signature result
 */
public class ITextSignatureResult extends SignatureResult {
	
	private PdfPKCS7 pdfPKCS7;
	
	private ITextSignatureResult(PdfPKCS7 pkcs7) {
		super(pkcs7.getSigningCertificate(), computeSigningTime(pkcs7), isSigningTimeFromTimeStampToken(pkcs7), pkcs7.getReason(), pkcs7
				.getLocation());
		this.pdfPKCS7 = pkcs7;
	}
	
	private ITextSignatureResult() {
	}
	
	private static boolean isSigningTimeFromTimeStampToken(PdfPKCS7 pkcs7) {
		return pkcs7.getTimeStampToken() != null;
	}
	
	private static Date computeSigningTime(PdfPKCS7 pkcs7) {
		if (pkcs7.getTimeStampToken() == null) {
			return pkcs7.getSignDate().getTime();
		} else {
			// FIXME get sure the timestamp signature is completely verified!!, trust could be checked against the same input trusted anchors (as it happens with PAdES-4 Document Time-stamps) see com.itextpdf.text.pdf.PdfPKCS7.verifyTimestampImprint()
			return pkcs7.getTimeStampToken().getTimeStampInfo().getGenTime();
		}
	}
	
	/**
	 * @return iText specific PdfPKCS7 for a specific signature or null if it isn't available (e.g. for some INVALID
	 *         cryptograhpic signatures)
	 */
	public PdfPKCS7 getPdfPKCS7() {
		return pdfPKCS7;
	}
	
	/**
	 * created with the purpose to help maintaing the inmutability of the ITextSignatureResult instance, and the ease
	 * to 'build' it before instantiation
	 */
	// TODO evaluate if this builder is ok here, or it should belong to SignatureResult... and be extended by this
	// class?? check GoF for guidelines
	public static class ITextSignatureResultBuilder {
		
		private static final Logger logger = Logger.getLogger(ITextSignatureResultBuilder.class);
		
		private PdfPKCS7 pkcs7;
		private RevocationStatus revocationStatus;
		
		private TrustStatus trustStatus;
		private ValidityStatus validityStatus;
		private CryptographicSignatureStatus cryptographicSignatureStatus;
		private InputStream signedVersionDocument;
		private SignatureResult counterSignature;
		private String signatureName;
		
		public ITextSignatureResultBuilder setPkcs7(PdfPKCS7 pkcs7) {
			this.pkcs7 = pkcs7;
			return this;
		}
		
		public ITextSignatureResultBuilder setRevocationStatus(RevocationStatus revocationStatus) {
			this.revocationStatus = revocationStatus;
			return this;
		}
		
		public ITextSignatureResult build() {
			ITextSignatureResult iTextSignatureResult;
			if (pkcs7 != null) {
				iTextSignatureResult = new ITextSignatureResult(pkcs7);
			} else {
				// TODO we have to ensure that if cryptographic signature is wrong, no signer status information is
				// returned as adobe reader/acrobat does
				iTextSignatureResult = new ITextSignatureResult();
			}
			iTextSignatureResult.setSignatureName(signatureName);
			
			iTextSignatureResult.setCrytographicSignatureStatus(cryptographicSignatureStatus);
			
			if (revocationStatus != null) {
				iTextSignatureResult.getSignerVerificationStatus().setRevocationStatus(revocationStatus);
			} else {
				iTextSignatureResult.getSignerVerificationStatus().setRevocationStatus(
						new RevocationStatus(RevocationStatus.Status.UNKNOWN, I18N.getString("revocation.status.is.unknown"), I18N
								.getString("revocation.status.is.unknown.fullmessage")));
			}
			
			if (validityStatus != null) {
				iTextSignatureResult.getSignerVerificationStatus().setValidityStatus(validityStatus);
			} else {
				iTextSignatureResult.getSignerVerificationStatus().setValidityStatus(
						new ValidityStatus(ValidityStatus.Status.UNKNOWN, I18N.getString("validity.status.is.unknown")));
			}
			
			// TODO consider to set the UNKNOWN status for trust here, if it gets as 'null' in this construction
			// phase, is it even possible?
			iTextSignatureResult.getSignerVerificationStatus().setTrustStatus(trustStatus);
			
			iTextSignatureResult.setSignedVersionDocument(signedVersionDocument);
			
			iTextSignatureResult.setCounterSignature(counterSignature);
			
			return iTextSignatureResult;
		}
		
		public ITextSignatureResultBuilder setTrustStatus(SignatureResult.TrustStatus trusted) {
			this.trustStatus = trusted;
			return this;
		}
		
		public TrustStatus getTrustStatus() {
			return trustStatus;
		}
		
		public ITextSignatureResultBuilder setValidityStatus(ValidityStatus validityStatus) {
			this.validityStatus = validityStatus;
			return this;
		}
		
		public ITextSignatureResultBuilder setCryptographicSignatureStatus(CryptographicSignatureStatus cryptographicSignatureStatus) {
			this.cryptographicSignatureStatus = cryptographicSignatureStatus;
			return this;
		}
		
		public void setSignedVersionDocument(InputStream signedVersionDocument) {
			this.signedVersionDocument = signedVersionDocument;
		}
		
		public void setCounterSignature(SignatureResult counterSignature) {
			this.counterSignature = counterSignature;
		}
		
		public void setSignatureName(String signatureName) {
			this.signatureName = signatureName;
		}
		
	}
}
