/**
 * OpenKM, Open Document Management System (http://www.openkm.com)
 * Copyright (c) 2006-2013 Paco Avila & Josep Llort
 * 
 * No bytes were intentionally harmed during the development of this application.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package pe.blobfish.signature;

import java.io.InputStream;
import java.security.cert.X509Certificate;
import java.util.Date;

import org.apache.log4j.Logger;
import pe.blobfish.certutils.CertificateUtil;

/**
 * SignatureResult class. For details on validation outputs see:
 * {@link SignatureResult#getCryptographicSignatureStatus()} and {@link SignatureResult#getSignerVerificationStatus()},
 * currently the former can be invalidated by unknown or
 * undetermined errors, handled by a global catch in OKMITextPDFSignature.
 * 
 */
// TODO evaluate to inform about the fact that this is a SignatureResult for a 'Document Time-stamp' (ETSI TS 102 778-4 V1.1.1, A.2), see com.itextpdf.text.pdf.security.PdfPKCS7.isTsp.
public abstract class SignatureResult {
	
	private final Logger logger = Logger.getLogger(getClass());
	
	// some holders for information TODO evaluate to move them to the appropiate status outputs
	private Date signingTime;
	private X509Certificate signer;
	private boolean signingTimeFromTimeStampToken;
	private String signingReason;
	private String signingLocation;
	
	// a variable to hold certification path validation (See RFC 5280 sec. 6) output status
	private SignerStatus signerVerificationStatus = new SignerStatus();
	
	// a variable to hold status verification on cryptographic signature, e.g. RSA signature (currentlly includes
	// signature format validation status too
	private CryptographicSignatureStatus cryptographicSignatureStatus = null;
	
	// TODO evaluate to create a GeneralSignatureResultStatus to place global catch errors there, possible statuses
	// would be OK, ERROR, it doesn't matter if the API ends up being not to simple... it seems to be neccessary to
	// include al required validation data
	
	/**
	 * TODO document an input stream containing the PDF actually signed??
	 */
	private InputStream signedVersionDocument;
	private SignatureResult counterSignature;
	
	// TODO evaluate to replace it by protected field
	void setSignatureName(String signatureName) {
		this.signatureName = signatureName;
	}
	
	// TODO evaluate to make it protected, consider that this is a regular PDF signature 'property'
	private String signatureName;
	
	/**
	 * SignatureResult
	 * 
	 * @param signingTime the signing time (local or from timeStampToken)
	 * @param signingTimeFromTimeStampToken true if the signing time comes from a timeStampToken
	 * @param signingReason
	 * @param signingLocation
	 */
	protected SignatureResult(X509Certificate signerCertificate, Date signingTime, boolean signingTimeFromTimeStampToken,
			String signingReason, String signingLocation) {
		this.signer = signerCertificate;
		this.signingTime = signingTime;
		this.signingTimeFromTimeStampToken = signingTimeFromTimeStampToken;
		this.signingReason = signingReason;
		this.signingLocation = signingLocation;
	}
	
	protected SignatureResult() {
	}
	
	public CryptographicSignatureStatus getCryptographicSignatureStatus() {
		return cryptographicSignatureStatus;
	}
	
	void setCrytographicSignatureStatus(CryptographicSignatureStatus cryptographicSignatureStatus) {
		if (cryptographicSignatureStatus.getStatus() == CryptographicSignatureStatus.Status.INVALID) {
			logger.warn("Invalidated SignatureResult: " + cryptographicSignatureStatus.getMessage());
		}
		this.cryptographicSignatureStatus = cryptographicSignatureStatus;
	}
	
	public SignerStatus getSignerVerificationStatus() {
		
		return signerVerificationStatus;
	}
	
	// TODO check inmutability of this class state!! this setters could be package protected?
	public void setSignedVersionDocument(InputStream signedVersionDocument) {
		this.signedVersionDocument = signedVersionDocument;
	}
	
	public void setCounterSignature(SignatureResult counterSignature) {
		this.counterSignature = counterSignature;
	}
	
	public SignatureResult getCounterSignature() {
		return counterSignature;
	}
	
	// TODO determine if we should allow to access to it for bad cryptographic signatures, unit test it
	public InputStream getSignedVersionDocument() {
		return signedVersionDocument;
	}
	
	/**
	 * The AcroForm field name (/T entry).
	 * 
	 * @return
	 */
	public String getSignatureName() {
		return signatureName;
	}
	
	// TODO evualte to move it to an independent class
	// TODO evaluate to name it: CertPathValidationStatus
	public final static class SignerStatus {
		
		private TrustStatus trustStatus;
		
		// TODO none of these should be validated and filled if trust status is 'UNTRUSTED'
		private ValidityStatus validityStatus;
		private RevocationStatus revocationStatus;
		
		public TrustStatus getTrustStatus() {
			return trustStatus;
		}
		
		void setTrustStatus(TrustStatus trustStatus) {
			this.trustStatus = trustStatus;
		}
		
		public ValidityStatus getValidityStatus() {
			return this.validityStatus;
		}
		
		void setValidityStatus(ValidityStatus validityStatus) {
			this.validityStatus = validityStatus;
		}
		
		public RevocationStatus getRevocationStatus() {
			return revocationStatus;
		}
		
		void setRevocationStatus(RevocationStatus revocationStatus) {
			this.revocationStatus = revocationStatus;
		}
		
		// TODO evaluate to create a toString method for all of these status encapsulation classes, maybe in the
		// FriendlyMessageStatus, but consider that at least the current status class doesn't subclass it.
	}
	
	static abstract class FriendlyMessageStatus {
		
		protected String message;
		protected String detailedMessage;
		
		public FriendlyMessageStatus(String message, String detailedMessage) {
			this.message = message;
			this.detailedMessage = detailedMessage;
		}
		
		// TODO evaluate to support collection messages for some these *Status
		public String getMessage() {
			return message;
		}
		
		public String getDetailedMessage() {
			return detailedMessage;
		}
	}
	
	public final static class RevocationStatus extends FriendlyMessageStatus {
		
		RevocationStatus(Status status, String message, String detailedMessage) {
			super(message, detailedMessage);
			this.status = status;
		}
		
		private Status status;
		
		public Status getStatus() {
			return status; // To change body of created methods use File | Settings | File Templates.
		}
		
		// TODO evaluate to support collection of revocation statuses, for example, for each certificate
		public enum Status {
			/**
			 * Not revoked at signing time
			 */
			OK,
			
			/**
			 * Revoked (or potentially revoked) at signing time
			 */
			REVOKED,
			
			/**
			 * Warning: For verification purposes this status should be considered equivalent to
			 * {@link SignatureResult.RevocationStatus.Status#REVOKED}.
			 */
			UNKNOWN
		}
	}
	
	public final static class TrustStatus extends FriendlyMessageStatus {
		
		private Status status;
		
		TrustStatus(Status status, String message, String detailedMessage) {
			super(message, detailedMessage);
			this.status = status;
		}
		
		public Status getStatus() {
			return status;
		}
		
		// TODO consider that for each of these statuses there should be a corresponding appropiate CUSTOMIZED ICON
		// in the GUI in the way adobe reader/acrobat does
		// TODO look if adobe always determine trust only after cryptographic validity
		public enum Status {
			TRUSTED, UNKNOWN, UNTRUSTED
		}
	}
	
	public final static class ValidityStatus extends FriendlyMessageStatus {

		ValidityStatus(Status status, String message) {
			super(message, null);
			this.status = status;
		}
		
		private Status status;
		
		public Status getStatus() {
			return this.status;
		}
		
		@Override
		public String getDetailedMessage() {
			throw new UnsupportedOperationException();
		}
		
		// TODO evaluate a something to generalize these Status enums, the UNKNOWN status would be 'generalizable'?
		public enum Status {
			VALID, UNKNOWN, EXPIRED, NOT_YET_VALID
		}
	}
	
	public static class CryptographicSignatureStatus extends FriendlyMessageStatus {
		
		private Status status;

		CryptographicSignatureStatus(Status status, String message, String detailedMessage) {
			super(message, detailedMessage);
			this.status = status;
		}
		
		public Status getStatus() {
			return this.status;
		}
		
		@Override
		public String getDetailedMessage() {
			throw new UnsupportedOperationException();
		}
		
		/**
		 * VALID if the cryptographic signature verification operation (e.g. RSA) succeeds with the public key intended
		 * for the verification (e.g. CMS SignerInfo TODO detail this verification mechanism) .
		 * 
		 * TODO to evaluate:
		 *       - VALID if the CMS/PKCS #7 included in the PDF has a right structure and is not corrupted
		 * 
		 *       INVALID if any of the conditions above is invalid or if anything else fails (a global catch has been
		 *       implemented)
		 */
		public enum Status {
			VALID, INVALID
		}
		
	}
	
	/**
	 * The signer certificate included in the Signature object.
	 * 
	 * @return the signer certificate or null if it isn't available
	 * @should return the signer certificate only for trusted cert paths
	 */
	public X509Certificate getSignerCertificate() {
		return signer;
	}
	
	// TODO check if the following data is being returned by Adobe Acrobat/Reader for untrusted cert paths, maybe
	// some of the data is ok for untrusted paths but for invalid cryptographic signature no data should be available,
	// the same for revoked certificates... check it better
	
	/**
	 * 
	 * @return the Signing time if it is available or null
	 * @should return the timestamp token time if there is one available
	 */
	public Date getSigningTime() {
		return signingTime;// TODO check is Date inmutable? if it isn't return a copy
	}
	
	public boolean isSigningTimeFromTimeStampToken() {
		return signingTimeFromTimeStampToken;
	}
	
	public String getSigningReason() {
		return this.signingReason;
	}
	
	public String getSigningLocation() {
		return this.signingLocation;
	}
	
	// TODO evaluate to create method to get issuer and subject fields in a friendly fashion, e.g. commonName,
	// Organization for any certificate, see Acrobat Reader way to do this.
	
	// TODO evaluate to create a method to get the timestamptoken signer certicate
	
	/**
	 * 
	 * @return a descriptive string for the current signature
	 * @should provide a descriptive string for the current signature
	 */
	@Override
	public String toString() {
		
		if (cryptographicSignatureStatus != null && cryptographicSignatureStatus.getStatus() == CryptographicSignatureStatus.Status.VALID) {
			// TODO consider to check for trust status too before asserting the name of the signer, because after all
			// names only exist in the context of trusted anchors
			return I18N.getString("signed.by", getFriendlySubjectName());
		} else {
			// TODO check if adobe reader displays warnings about presented data when signature is invalid, then
			// mimic them.
			String s;
			if (signer != null) {
				s = I18N.getString("purported.signer", getFriendlySubjectName());
			} else {
				s = I18N.getString("signer.is.unknown");
			}
			
			return I18N.getString("invalid.signature.message", s);
		}
	}
	
	private String getFriendlySubjectName() {
        // TODO evaluate to default to the full DN.
		String subjectName = I18N.getString("unknown");
		
		String value = CertificateUtil.getSubjectName(signer);
		
		if (value != null) {
			subjectName = value;
		}
		return subjectName;
	}
	
}
