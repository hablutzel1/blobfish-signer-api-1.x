/**
 * OpenKM, Open Document Management System (http://www.openkm.com)
 * Copyright (c) 2006-2013 Paco Avila & Josep Llort
 * 
 * No bytes were intentionally harmed during the development of this application.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package pe.blobfish.signature;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Date;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;

public class SignatureResultTest extends BaseTests {
	
	/**
	 * @verifies provide a descriptive string for the current signature
	 * @see SignatureResult#toString()
	 */
	@Test
	public void toString_shouldProvideADescriptiveStringForTheCurrentSignature() throws Exception {
		
		assertToStringPrefixForValidadedResource("/adbe_pkcs7_detached_sha256_valid_signature_untrusted_signer.pdf",
				CoreMatchers.startsWith("Signed by: "));
		
		// TODO for this type of corrupted signatures research if it is possible to get the purported signer. From
		// the UI, adobe can display the name of the purported signer in the graphic stamp, research if the graphic
		// stamp is not more than only a graphic
		assertToStringPrefixForValidadedResource("/adbe_pkcs7_detached_sha256_corrupted_signature_untrusted_signer.pdf",
				CoreMatchers.startsWith("Invalid signature"));
		
		// test for a invalid signature with the signer certificate available in the PDF but not in the signature result
		assertToStringPrefixForValidadedResource("/adbe_pkcs7_detached_sha256_invalid_signature_untrusted_signer.pdf",
				CoreMatchers.allOf(CoreMatchers.startsWith("Invalid signature"), CoreMatchers.containsString("Unknown")));
		
	}
	
	private void assertToStringPrefixForValidadedResource(String classpathResource, Matcher<? super String> matcher) throws KeyStoreException,
			CertificateException, NoSuchAlgorithmException, IOException {
		
		SignatureResult validSignature = verifyClasspathResource(classpathResource);
		String actual = validSignature.toString();
		System.out.println("output>> " + actual);
		assertThat(actual, matcher);
	}
	
	/**
	 * @verifies return the timestamp token time if there is one available
	 * @see SignatureResult#getSigningTime()
	 */
	@Test
	public void getSigningTime_shouldReturnTheTimestampTokenTimeIfThereIsOneAvailable() throws Exception {
		ITextSignatureResult signatureResult = (ITextSignatureResult) verifyClasspathResource("/adbe_pkcs7_detached_sha1_with_timestamp_valid_signature_untrusted_signer.pdf");
		
		Date signingTime = signatureResult.getSigningTime();
		// expected 2013 09 16 13 54 38 GMT-5
		DateTime dateTime = new DateTime(2013, 9, 16, 13, 54, 38, 0, DateTimeZone.forOffsetHours(-5));
		assertEquals(signingTime, dateTime.toDate());
		assertTrue(signatureResult.isSigningTimeFromTimeStampToken());
		
	}
}
