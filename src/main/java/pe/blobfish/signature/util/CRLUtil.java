/**
 * OpenKM, Open Document Management System (http://www.openkm.com)
 * Copyright (c) 2006-2013 Paco Avila & Josep Llort
 * 
 * No bytes were intentionally harmed during the development of this application.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package pe.blobfish.signature.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;

import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.x509.*;

public class CRLUtil {

    // region Copied (and patched) from iText itext 5.1.3 com.itextpdf.text.pdf.PdfPKCS7.getCrlUrl()
	private static ASN1Primitive getExtensionValue(X509Certificate cert, String oid) throws IOException {
		byte[] bytes = cert.getExtensionValue(oid);
		if (bytes == null) {
			return null;
		}
		ASN1InputStream aIn = new ASN1InputStream(new ByteArrayInputStream(bytes));
		ASN1OctetString octs = (ASN1OctetString) aIn.readObject();
		aIn = new ASN1InputStream(new ByteArrayInputStream(octs.getOctets()));
		return aIn.readObject();
	}
	
	public static String getCrlUrl(X509Certificate certificate) throws CertificateParsingException {
		try {
			ASN1Primitive obj = getExtensionValue(certificate, X509Extensions.CRLDistributionPoints.getId());
			if (obj == null) {
				return null;
			}
			CRLDistPoint dist = CRLDistPoint.getInstance(obj);
			DistributionPoint[] dists = dist.getDistributionPoints();
			for (DistributionPoint p : dists) {
				DistributionPointName distributionPointName = p.getDistributionPoint();
				if (DistributionPointName.FULL_NAME != distributionPointName.getType()) {
					continue;
				}
				GeneralNames generalNames = (GeneralNames) distributionPointName.getName();
				GeneralName[] names = generalNames.getNames();
				for (GeneralName name : names) {
					if (name.getTagNo() != GeneralName.uniformResourceIdentifier) {
						continue;
					}
					DERIA5String derStr = DERIA5String.getInstance((ASN1TaggedObject) name.toASN1Primitive(), false);
					return derStr.getString();
				}
			}
		} catch (Exception e) {
		}
		return null;
	}
	// endregion
}
