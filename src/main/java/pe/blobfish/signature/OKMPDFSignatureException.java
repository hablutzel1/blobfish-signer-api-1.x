package pe.blobfish.signature;

/// TODO define exception hierarchy as needed, first the basic exception
public class OKMPDFSignatureException extends RuntimeException {

    public OKMPDFSignatureException(Throwable e) {
        super(e);
    }

    /// TODO should provide an alternative friendly method to get the exception message without any class reference string (end user doesn't need to know we work with java)
}
