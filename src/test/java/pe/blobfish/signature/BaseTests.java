/**
 * OpenKM, Open Document Management System (http://www.openkm.com)
 * Copyright (c) 2006-2013 Paco Avila & Josep Llort
 * 
 * No bytes were intentionally harmed during the development of this application.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package pe.blobfish.signature;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;

// TODO configure once and here the log4j devel configuration
public class BaseTests {
	
	protected static SignatureResult verifyClasspathResource(String classpathResource) throws KeyStoreException, CertificateException,
			NoSuchAlgorithmException, IOException {
		OKMITextPDFSignature verifier = instantiateOKMITextPDFSignature();
		return verifier.verify(SignatureResultTest.class.getResourceAsStream(classpathResource));
	}
	
	protected static OKMITextPDFSignature instantiateOKMITextPDFSignature() throws KeyStoreException, CertificateException,
			NoSuchAlgorithmException, IOException {
		KeyStore jks = KeyStore.getInstance("JKS");
		jks.load(BaseTests.class.getResourceAsStream("/truststore_with_root_key.jks"), "changeit".toCharArray());
		Set<X509Certificate> trustAnchors = new HashSet<X509Certificate>();
		trustAnchors.add((X509Certificate) jks.getCertificate("test_trusted_root_ca"));
		return new OKMITextPDFSignature(trustAnchors);
	}
	
	@Before
	public void baseSetUp() throws Exception {
		
		// TODO evaluate if this is right and doesn't interfere with any functionality to install BC provider in this
		// way, see OKMITextPDFSignature#OKMITextPDFSignature
		Security.addProvider(new BouncyCastleProvider());
		
		Locale.setDefault(Locale.ENGLISH); // because some tests expect some strings in englis
		// Locale.setDefault(new Locale("es")); // for looking i18n in action: TODO tests strings matchers should be
		// made i18n aware, maybe making them aware of I18N class?
	}
}
