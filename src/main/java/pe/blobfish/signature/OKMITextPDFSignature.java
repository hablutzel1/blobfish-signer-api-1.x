/**
 * OpenKM, Open Document Management System (http://www.openkm.com)
 * Copyright (c) 2006-2013 Paco Avila & Josep Llort
 * 
 * No bytes were intentionally harmed during the development of this application.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package pe.blobfish.signature;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.cert.*;
import java.security.cert.Certificate;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.itextpdf.text.pdf.security.PdfPKCS7;
import pe.blobfish.signature.util.CRLUtil;
import org.apache.log4j.Logger;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;

/**
 * iText specific implementation.
 */
public class OKMITextPDFSignature implements OKMPDFSignature {
	
	private static final Logger logger = Logger.getLogger(OKMITextPDFSignature.class);

    // TODO note that SimpleDateFormat is not thread safe, check if it is really ok to use it as an instance variable, maybe we should mark 'OKMITextPDFSignature' as not thread safe too
	private final SimpleDateFormat defaultDateFormat;
	
	private static boolean libraryInitialized = false; // TODO ensure it is thread safe
	private Set<X509Certificate> x509TrustAnchors;

    // TODO create another constructor that receives a KeyStore File with its corresponding password for easier usage (this time check what is the password used for... integrity only?).
	public OKMITextPDFSignature(Set<X509Certificate> x509TrustAnchors) {
		this.x509TrustAnchors = x509TrustAnchors;
		
		// Current itext version is requiring BC provider installed because the way it requests MessageDigest instances
		// (maybe other reasons too), currently it gets the algorithm name using its own oid/name translation table in
		// com.itextpdf.text.pdf.PdfPKCS7, so for example 2.16.840.1.101.3.4.2.1 gets translated to SHA256 which is not
		// available with the following call 'MessageDigest.getInstance("SHA256")' if the BC provider is not installed.
		
		// TODO evaluate if we really need (after updating or patching iText or updating the digest algorithm
		// aliases, etc) or want to REQUIRE that the BC provider is installed. maybe we should make it optional to make
		// it easier to use this library and avoid BC jars hell!.
		
		if (!libraryInitialized) { // TODO evaluate this lazy style library loader or something like
									// PDFSignature.init(), maybe this should be in a higher abstract class
			if (Security.getProvider("BC") != null) {
				logger.warn("BC provider is already installed, using that version instead (some problems may arise)");
			} else {
				Security.addProvider(new BouncyCastleProvider());
			}
			libraryInitialized = true;
		}
		
		defaultDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // TODO evaluate to use a field initializer,
																			// or maybe it is ok as later the format
																			// should come from some locale specific
																			// configuration
	}
	
	@Override
	public SignatureResult verify(byte[] input) {
		return verify(new ByteArrayInputStream(input));
	}
	
	@Override
	// TODO define supported PDF signature formats/SubFilters in documentation, see iText
	// TODO document if this method is responsible for closing the passed 'InputStream'
	public SignatureResult verify(InputStream is) {
		// code currently procedural, TODO move to object oriented as needed, think in: reuse, polymorphism
		
		try {
			PdfReader reader;
			
			reader = new PdfReader(is); // with this version of itext the InputStream gets converted immediately to a
										// byte array with a memory impact, see
										// com.itextpdf.text.pdf.RandomAccessFileOrArray.InputStreamToArray() this is
										// the method that gets used, TODO investigate if there exist such thing as
										// support for really big files proccesed in a stream fashion, maybe with a
										// newer itext version? because after all the PDF spec makes multiple mentions
										// of
										// random access to get performance benefits and reduce memory impact too?
			
			AcroFields iTextAcroFields = reader.getAcroFields();
			
			ArrayList<String> names = iTextAcroFields.getSignatureNames(); // one element per different signature form
																			// field
			// (/T entry) TODO check if there are signatures in
			// places different than forms
			
			if (names.isEmpty()) {
				return null; // no signature found
			}
			
			// order signatures by its revision in ascendent fashion
			ArrayList<Object[]> sortedSignatureFields = new ArrayList<Object[]>();
			for (String name : names) {
				sortedSignatureFields.add(new Object[] { iTextAcroFields.getRevision(name), name });
			}
			Collections.sort(sortedSignatureFields, new Comparator<Object[]>() {
				@Override
				public int compare(Object[] o1, Object[] o2) {
					return (Integer) o1[0] - (Integer) o2[0];
				}
			});

			logger.warn("itext reports following number of revisions to PDF document " + iTextAcroFields.getTotalRevisions());
			
			return recurse(iTextAcroFields, sortedSignatureFields, 1);
			
		} catch (Exception e) { // any other itext generated exception, not related to signature processing
            /// TODO get sure this catch will catch exceptions only for non-signature related problems
			throw new OKMPDFSignatureException(e);
		}
	}
	
	// TODO double check that signature result processing is really independent for each signature result, consider
	// exception management too
	private SignatureResult recurse(AcroFields iTextAcroFields, ArrayList<Object[]> sortedRevisions, int curRevision) throws IOException {
		
		// silly recursion??
		SignatureResult counterSignatureResult = null;
		if (sortedRevisions.size() < curRevision) {
			// stop recursion
			return null;
		} else {
			counterSignatureResult = recurse(iTextAcroFields, sortedRevisions, curRevision + 1);
		}
		
		String signatureFieldName = (String) sortedRevisions.get(curRevision - 1)[1];
		logger.debug("===== " + signatureFieldName + " =====");
		
		// take into consideration that it seems 'signatureCoversWholeDocument' in this itext version has problems TODO
		// confirm it and migrate to a newer version if needed
		boolean iTextReportsSignatureForWholeDoc = iTextAcroFields.signatureCoversWholeDocument(signatureFieldName);
		if (iTextReportsSignatureForWholeDoc) {
			logger.warn("itext reports a signature that cover the whole document: \"" + signatureFieldName + "\"");
		}
		
		ITextSignatureResult.ITextSignatureResultBuilder signatureResultBuilder = new ITextSignatureResult.ITextSignatureResultBuilder();
		
		signatureResultBuilder.setSignatureName(signatureFieldName);
		signatureResultBuilder.setCounterSignature(counterSignatureResult);
		
		try {

            PdfPKCS7 pdfPKCS7 = iTextAcroFields.verifySignature(signatureFieldName, "BC");

            boolean isCryptographicSignatureValid = pdfPKCS7.verify();
			// TODO validate signer for invalid signatures too? Adobe reader/acrobat doesn't seem to do this
			if (isCryptographicSignatureValid) { // certification path validation only for valid cryptographic
													// signatures
				
				// add the partial or full signed content to each signature result
				// TODO get sure it is only being done for valid cryptographic signatures and determine what other
				// requirements should be needed to put this data available
				signatureResultBuilder.setSignedVersionDocument(iTextAcroFields.extractRevision(signatureFieldName));

				signatureResultBuilder.setPkcs7(pdfPKCS7);
				signatureResultBuilder.setCryptographicSignatureStatus(new SignatureResult.CryptographicSignatureStatus(
						SignatureResult.CryptographicSignatureStatus.Status.VALID, I18N.getString("cryptographic.signature.is.valid"), I18N
								.getString("cryptographic.signature.is.valid.fullmessage")));
				
				X509Certificate[] signerCertificatePathFromPKCS7 = (X509Certificate[]) pdfPKCS7.getSignCertificateChain();
				// validate certificate at the time of signing (purported or from timestamp)
				Calendar verificationTime;
				if (pdfPKCS7.getTimeStampToken() != null) {
					// FIXME only if the timestamp is considered valid and coming from a trusted TSA
					verificationTime = pdfPKCS7.getTimeStampDate();
				} else {
					verificationTime = pdfPKCS7.getSignDate(); // the signature dictionary 'M' entry?
					// from PDF 32000-1:2008 Table 252 (Optional) The time of signing. Depending on the
					// signature handler, this may be a normal unverified computer time or a time generated in a
					// verifiable way from a secure time server.... TODO so we could expect it to be the
					// timestamp date?? when there is a tst? test it!
				}
				
				// region configuring trust anchors
				HashSet<TrustAnchor> trustAnchorsSet = new HashSet<TrustAnchor>();
				for (X509Certificate trustAnchor : x509TrustAnchors) {
					// TODO determine if name constraints usage apply for our purposes
					trustAnchorsSet.add(new TrustAnchor(trustAnchor, null));
				}
				// endregion
				
				// will be switch off if no trust or difficulties getting
				// appropiate CRLs
				boolean cpvRevocationChecking = true;
				
				// if CertPathBuilder failed for reason not being
				// handled in its 'catch' clause.
				boolean tryAlternativePathBuilding = false;
				
				// TODO look for an alternative to CertPathBuilder (or modify it) to stop any checking for
				// validity (expiration, etc) while building a cert path, take into consideration that validity
				// could be used to get the appropiate CA in cases where there is more than one candidate CA,
				// confirm this in BC CertPathBuilder implementation
				
				// region constructing a possible certification path
				// TODO determine if this is right to use both a CertPathBuilder and a CertPathValidator,
				// doesn't the first of them make the full work??
				// TODO check if there is any way to disable some checks for the CertPathBuilder like certs
				// validity
				// TODO check how does this implementation behave if there is more than one possible path, is
				// it able to return several paths?
				CertPathBuilder certPathBuilder = CertPathBuilder.getInstance("PKIX", "BC");
				X509CertSelector signerSelector = new X509CertSelector();
				signerSelector.setCertificate(signerCertificatePathFromPKCS7[0]); // signer certificate
				PKIXBuilderParameters certPathBuilderParams = new PKIXBuilderParameters(trustAnchorsSet, signerSelector);
				CertStore certStoreFromPKCS7 = CertStore.getInstance("Collection",
						new CollectionCertStoreParameters(Arrays.asList(signerCertificatePathFromPKCS7)), "BC");
				certPathBuilderParams.addCertStore(certStoreFromPKCS7);
				certPathBuilderParams.setRevocationEnabled(false); // we just want the certification path built
				
				// to make this CertPathBuilder not to fail if the CertPathValidator wouldn't fail because of
				// certificate validity
				certPathBuilderParams.setDate(verificationTime.getTime());
				certPathBuilderParams.addCertPathChecker(new TimestampingExtendedKeyUsageChecker());
				CertPathBuilderResult certPathBuilderResult = null;
				try {
					certPathBuilderResult = certPathBuilder.build(certPathBuilderParams);
				} catch (CertPathBuilderException e) {
					if (e.getMessage().equals("Unable to find certificate chain.")
							|| e.getMessage().equals("No issuer certificate for certificate in certification path found.")) {
						cpvRevocationChecking = false;
						// set untrusted path
						signatureResultBuilder.setTrustStatus(
								new SignatureResult.TrustStatus(SignatureResult.TrustStatus.Status.UNTRUSTED, I18N
										.getString("signer.certificate.is.not.trusted"), I18N
										.getString("signer.certificate.is.not.trusted.fullmessage")))
						
						.setRevocationStatus(
								new SignatureResult.RevocationStatus(SignatureResult.RevocationStatus.Status.UNKNOWN, I18N
										.getString("unable.to.process.revocation.status.because.signer.is.not.trusted"), I18N
										.getString("unable.to.process.revocation.status.because.signer.is.not.trusted.fullmessage")));
					} else {
						// this CertPathBuilder is being used only for building a certification path, other
						// errors are being appropiately being validated by the CertPathValidator, because of
						// that we create 'tryAlternativePathBuilding'
						tryAlternativePathBuilding = true;
					}
					
				}
				
				// TODO research if certpath java implementation does support to include the trust anchor in
				// the CertPath
				CertPath certPathForCPV = null;
				
				CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
				
				if (certPathBuilderResult != null) { // path built up to a trust anchor
					certPathForCPV = certPathBuilderResult.getCertPath();
					signatureResultBuilder.setTrustStatus(new SignatureResult.TrustStatus(SignatureResult.TrustStatus.Status.TRUSTED, I18N
							.getString("signer.certificate.is.trusted"), I18N.getString("signer.certificate.is.trusted.fullmessage")));
				} else {
					
					if (tryAlternativePathBuilding) {
						// there is still hope to build a valid trusted certification path
						// this block would be required only if the CertPathBuilder failed because of reasons
						// other than building certification path and we finally let the CPV to validate this
						// path
						certPathForCPV = certificateFactory.generateCertPath(Arrays.asList(signerCertificatePathFromPKCS7));
						
						// TODO we have to set trust status somewhere
						// signatureResultBuilder.setTrustStatus(new SignatureResult.TrustStatus(
						// SignatureResult.TrustStatus.Status.UNTRUSTED,
						// I18N.getString("signer.certificate.is.not.trusted"), I18N
						// .getString("signer.certificate.is.not.trusted.fullmessage")));
					} else {
						
						// NOTE: just for allowing CertPathValidator to do its work
						if (signerCertificatePathFromPKCS7.length == 1) {
							certPathForCPV = certificateFactory.generateCertPath(Arrays.asList(signerCertificatePathFromPKCS7));
						} else {
							certPathForCPV = certificateFactory.generateCertPath(Arrays.asList(signerCertificatePathFromPKCS7).subList(0,
									signerCertificatePathFromPKCS7.length - 1));
						}
						trustAnchorsSet
								.add(new TrustAnchor(signerCertificatePathFromPKCS7[signerCertificatePathFromPKCS7.length - 1], null));
					}
				}
				// endregion
				
				PKIXParameters certPathValidatorParams = new PKIXParameters(trustAnchorsSet);
				
				// revocation checking is currently dependant on trust status like in adobe reader/acrobat
				// only do CRLs related stuff if 'cpvRevocationChecking' is true
				if (cpvRevocationChecking) {
					// CRL searching for all certificates in certpath.
					List<? extends Certificate> certificates = certPathForCPV.getCertificates();
					for (Certificate curCertificate : certificates) {
						
						// get CRL dist point
						// TODO get CRLs in more than just this way, i.e. try other CRL URLs, search in
						// directories, etc. check RFC 5280 4.2.1.13
						// TODO if there is any distribution point with a protocol we can't process, emit a
						// warning. e.g. LDAP? or should we support it?
						String crlUrlString = CRLUtil.getCrlUrl((X509Certificate) curCertificate);
						if (crlUrlString == null) { // crl url unavailable
						
							cpvRevocationChecking = false;
							
							// TODO detail what is the certificate without the CDP
							signatureResultBuilder.setRevocationStatus(new SignatureResult.RevocationStatus(
									SignatureResult.RevocationStatus.Status.UNKNOWN,
									"Unable to get a valid CRL dist. point from at least one certificate", "TODO detail"));
							break;
						}
						
						CRL crl = downloadCRL(crlUrlString);
						
						if (crl != null) {
							Set<CRL> crlSet = new HashSet<CRL>();
							crlSet.add(crl);
							// TODO determine to use only one CertStore for all downloaded CRLs
							CertStore crlStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(crlSet), "BC");
							certPathValidatorParams.addCertStore(crlStore);
						} else {
							// we only check revocation if we have crls available for each cert in the path
							// display the user the warning indicating CRLs wasn't available at the time of
							// verification? maybe populating the unknown revocation status from here
							// TODO include details of the certificate for which the revocation status info
							// wasn't available
							signatureResultBuilder.setRevocationStatus(new SignatureResult.RevocationStatus(
									SignatureResult.RevocationStatus.Status.UNKNOWN, I18N
											.getString("unable.to.download.crl.for.at.least.one.certificate"), I18N
											.getString("unable.to.download.crl.for.at.least.one.certificate.fullmesage")));
							
							cpvRevocationChecking = false;
							
							break; // no more searching
						}
					}
				}
				
				certPathValidatorParams.setDate(verificationTime.getTime());
				
				try {
					
					CertPathValidator certPathValidator = CertPathValidator.getInstance("PKIX", "BC");
					certPathValidatorParams.setRevocationEnabled(cpvRevocationChecking);
					certPathValidatorParams.addCertPathChecker(new TimestampingExtendedKeyUsageChecker());
					CertPathValidatorResult validate = certPathValidator.validate(certPathForCPV, certPathValidatorParams);
					// TODO check: 'validate' is not being used at all? what does it contain?
					
					// OK only if revocation checking was enabled
					if (certPathValidatorParams.isRevocationEnabled()) {
						
						// TODO evaluate to include in the
						// 'certification.path.revocation.status.is.ok.fullmessage' some details about the
						// revocation status verification process. e.g. the time under which the revocation
						// verification has been done (purported or from tst)
						signatureResultBuilder.setRevocationStatus(new SignatureResult.RevocationStatus(
								SignatureResult.RevocationStatus.Status.OK, I18N.getString("certification.path.revocation.status.is.ok"),
								I18N.getString("certification.path.revocation.status.is.ok.fullmessage")));
					}
					
					// TODO currently this status value seems to be depending revocation status being OK,
					// check is this is the intended behaviour
					return signatureResultBuilder.setValidityStatus(
							new SignatureResult.ValidityStatus(SignatureResult.ValidityStatus.Status.VALID, I18N
									.getString("certificate.was.valid.at.the.time.of.signing"))).build();
				} catch (CertPathValidatorException e) {
					String exceptionMessage = e.getMessage();
					Matcher revocationMatcher = Pattern.compile("Certificate revocation after (.*), reason: (.*)")
							.matcher(exceptionMessage);
					if (revocationMatcher.find()) {
						// hardcoded locale because it is relying in java.util.Date.toString which always output in
						// english locale
						Date parsedDate = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy", Locale.US).parse(revocationMatcher.group(1));
						
						String reason = revocationMatcher.group(2);
						
						String revocationStatusMessage;
						if (e.getIndex() == 0) {
							revocationStatusMessage = I18N.getString("signer.certificate.revoked.at.date.and.reason",
									defaultDateFormat.format(parsedDate), reason);
						} else {
							revocationStatusMessage = I18N.getString("revoked.certificate.in.path.0.revoked.at.1.reason.2",
									((X509Certificate) e.getCertPath().getCertificates().get(e.getIndex())).getSubjectDN().toString(),
									defaultDateFormat.format(parsedDate), reason);
						}
						
						return signatureResultBuilder.setRevocationStatus(
								new SignatureResult.RevocationStatus(SignatureResult.RevocationStatus.Status.REVOKED,
										revocationStatusMessage, null)).build();
					} else if (exceptionMessage.startsWith("No CRLs found for issuer ")) {
						return signatureResultBuilder.setRevocationStatus(
								new SignatureResult.RevocationStatus(SignatureResult.RevocationStatus.Status.UNKNOWN, I18N.getString(
										"no.appropiate.crl.found.to.check.the.following.certificate", ((X509Certificate) e.getCertPath()
												.getCertificates().get(e.getIndex())).getSubjectDN().toString()), I18N
										.getString("no.appropiate.crl.found.to.check.the.following.certificate.fullmessage"))).build();
					} else if (exceptionMessage.startsWith("Could not validate certificate: certificate expired on")) {
						settingAsTrustedWithoutBeingSure(signatureResultBuilder);
						
						X509Certificate expiredCertificate = (X509Certificate) e.getCertPath().getCertificates().get(e.getIndex());
						return signatureResultBuilder.setValidityStatus(
								new SignatureResult.ValidityStatus(SignatureResult.ValidityStatus.Status.EXPIRED, I18N.getString(
										"certificate.was.expired.at.the.time.of.signing.expired.at.and.signature.produced.at",
										expiredCertificate.getSubjectDN().toString(),
										defaultDateFormat.format((expiredCertificate).getNotAfter()),
										defaultDateFormat.format(verificationTime.getTime())))).build();
						
					} else if (exceptionMessage.startsWith("Could not validate certificate: certificate not valid till")) {
						settingAsTrustedWithoutBeingSure(signatureResultBuilder);
						
						// test for certificate not yet valid situation
						X509Certificate notYetValidCertificate = (X509Certificate) e.getCertPath().getCertificates().get(e.getIndex());
						return signatureResultBuilder.setValidityStatus(
								new SignatureResult.ValidityStatus(SignatureResult.ValidityStatus.Status.NOT_YET_VALID, I18N.getString(
										"certificate.was.notyetvalid.at.the.time.of.signing.validfrom.and.signature.produced.at",
										notYetValidCertificate.getSubjectDN().toString(),
										defaultDateFormat.format((notYetValidCertificate).getNotBefore()),
										defaultDateFormat.format(verificationTime.getTime())))).build();
					} else if (exceptionMessage.startsWith("Trust anchor for certification path not found.")) {
						// will happen with ONE CERTIFICATE uncomplete certification paths
						// TODO determine: no need to set a appropiate status message??
						return signatureResultBuilder.build();
					} else {
						throw e; // rethrow
					}
				}
				
			} else {
				return signatureResultBuilder
						.setCryptographicSignatureStatus(
                                new SignatureResult.CryptographicSignatureStatus(
                                        SignatureResult.CryptographicSignatureStatus.Status.INVALID, I18N
                                        .getString("signature.digest.failed.to.validate"), ""))
						.setTrustStatus(
                                new SignatureResult.TrustStatus(SignatureResult.TrustStatus.Status.UNKNOWN, I18N
                                        .getString("trust.status.is.unknown"), I18N
                                        .getString("trust.status.is.unknown.because.invalid.signature.fullmessage"))).build();

			}
			
		} catch (Exception e) {
			logger.info("Error validating signature", e);
			// TODO on error provide as much information as possible in the signature. TODO check security
			// implications on doing so...altough after all if the signature is invalid the end user should be
			// taught that he shouldn't believe any data, data would be just for reference. create a ticket for
			// this and study it thoroughly.
			
			// TODO map common exceptions to our own error codes/messages.
			
			// TODO define this: it is important that all unhandled errors end up with an invalid signature,
			// but maybe not invalidating the cryptographic signature but the full property for the signature
			// result
			// status, so if we missed something the underlying libraries are throwing an exception for, at
			// least the verifier won't believe the signature is 'all right'... it is actually something
			// pesimistic
			// TODO do not include java class names in error messages, go up the exception causes removing class
			// names, we shouldn't produce error messages like the following:
			// "Some unknown error happened: java.lang.IllegalArgumentException: unknown tag value 10", because the user
			// doesn't need to know we work with Java
			return signatureResultBuilder
					.setCryptographicSignatureStatus(
							new SignatureResult.CryptographicSignatureStatus(SignatureResult.CryptographicSignatureStatus.Status.INVALID,
									"Some unknown error happened: " + e.getMessage(), ""))
					.setTrustStatus(
							new SignatureResult.TrustStatus(SignatureResult.TrustStatus.Status.UNKNOWN, I18N
									.getString("trust.status.is.unknown"), I18N.getString("trust.status.is.unknown.fullmessage"))).build();
			
		}
		
	}
	
	// FIXME: this code is dangerous, double, triple check
	private void settingAsTrustedWithoutBeingSure(ITextSignatureResult.ITextSignatureResultBuilder signatureResultBuilder) {
		if (signatureResultBuilder.getTrustStatus() == null) {
			// TODO verify: if we got here we have a trusted cert path??
			signatureResultBuilder.setTrustStatus(new SignatureResult.TrustStatus(SignatureResult.TrustStatus.Status.TRUSTED, "", ""));
		}
	}
	
	// TODO move to another class with responsability about revocation status checking
	private CRL downloadCRL(String crlUrlString) throws IOException, CertificateException, NoSuchProviderException, CRLException {
		// TODO evaluate to use a third party HTTP client implementation, like Apache HttpComponents, what for?
		// TODO allow to configure blobfish-signer-api to reach the internet through a
		// proxy
		URL crlUrl = new URL(crlUrlString);
		URLConnection crlUrlConnection = crlUrl.openConnection();
		InputStream crlInputStream;
		CRL crl;
		try {
			// TODO consider CRLs caching
			crlInputStream = crlUrlConnection.getInputStream();
			CertificateFactory crlCertificateFactory = CertificateFactory.getInstance("X.509", "BC");
			// TODO consider the situation where the downloaded 'crl' cannot be appropiately parsed, in this case
			// this problem should be notified to the user
			crl = crlCertificateFactory.generateCRL(crlInputStream);
		} catch (IOException e) {
			crl = null; // crl is null so: paramsPKIX.setRevocationEnabled(false)
		}
		return crl;
	}

	/**
	 * Intended to support time-stamping certificates (these have a critical extended key usage), see RFC 3161, 2.3:
	 *
	 * <pre>
	 * The corresponding certificate MUST contain only one instance of the
	 * extended key usage field extension as defined in [RFC2459] Section
	 * 4.2.1.13 with KeyPurposeID having value:
	 * id-kp-timeStamping. This extension MUST be critical.
	 * </pre>
	 *
	 * as these certificates are being expected for PAdES-4 'Document Time-stamp'.
	 * <p>
	 *
	 * For similar mechanism, see {@link com.itextpdf.text.pdf.security.CertificateVerification#verifyCertificate(java.security.cert.X509Certificate, java.util.Collection, java.util.Calendar)}.
	 */
	private static class TimestampingExtendedKeyUsageChecker extends PKIXCertPathChecker {
		@Override
		public void init(boolean forward) throws CertPathValidatorException {
		}

		@Override
		public boolean isForwardCheckingSupported() {
			return false;
		}

		@Override
		public Set<String> getSupportedExtensions() {
			// it seems it isn't ever called with the BC provider implementation
			return Collections.singleton(org.bouncycastle.asn1.x509.X509Extension.extendedKeyUsage.getId());
		}

		@Override
		public void check(Certificate cert, Collection<String> unresolvedCritExts) throws CertPathValidatorException {
			for (String unresolvedCritExt : unresolvedCritExts) {
				try {
					if (X509Extension.extendedKeyUsage.getId().equals(unresolvedCritExt)
							&& ((X509Certificate) cert).getExtendedKeyUsage().contains("1.3.6.1.5.5.7.3.8")) { // id-kp-timeStamping is present
						// we just needed to recognize this extension presence with 'id-kp-timeStamping', so we now consider this extension as processed, see RFC 5280, 4.2.1.12. Extended Key Usage.
						unresolvedCritExts.remove(X509Extension.extendedKeyUsage.getId());
					}
				} catch (CertificateParsingException e) {
					throw new CertPathValidatorException(e);
				}
			}
		}
	}
}
